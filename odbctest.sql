
drop table if exists fis_path;
drop table if exists fis_types;
drop table if exists fis_conv;


-- Test of attribute types

create table fis_types (
       path             varchar(4096) binary not null,
       st_dev           bigint unsigned not null,
       st_ino           bigint unsigned not null,
       is_dir           boolean not null,
       asmall           smallint not null,
       ausmall          smallint unsigned not null,
       ainteger         integer not null,
       auinteger        integer unsigned not null,
       atinyint         tinyint not null,
       autinyint        tinyint unsigned not null,
       tstamp           timestamp
);

insert into fis_types (path, st_dev, st_ino, is_dir, asmall, ausmall, ainteger, auinteger, atinyint, autinyint,
                       tstamp)
values                ("path", 1,    2,      true,   10,     11,      12,       13,        14,       15,
                       TIMESTAMP '2012^12^31 11*30*45');


create table fis_path (
       path             varchar(4096) binary not null,  
       st_dev           bigint unsigned not null,
       st_ino           bigint unsigned not null,
       is_dir           boolean not null
);

create index fis_idx_path1 on fis_path (path(150));
create index fis_idx_path2 on fis_path (st_dev, st_ino);


insert into fis_path values ("/foo/bar/baz", 123, 456, 1);



-- Test for conv when the attribute can be NULL

create table fis_conv (
       myint        integer,
       mytimestamp  timestamp,
       myfloat      float,
       myint64      bigint
);

insert into fis_conv values (123, timestamp '2024-01-06 19:07:00', 456.789, 1000000000000000);
insert into fis_conv values (null, null, null, null);
