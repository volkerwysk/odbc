#! /bin/bash
# This creates a database schema (relations) in the odbctest database.

mysql --user=odbctest --password=odbctest odbctest < odbctest.sql
