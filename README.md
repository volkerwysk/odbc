# Mercury ODBC Library

This is an ODBC library for the [Mercury](https://mercurylang.org) programming language. It lets you access
databases.

## Version

The latest version is 1.5.0, released 2024-11-23.

## The ODBC "extra" Library

This ODBC library is derived from the (old) ODBC "extra" library in the `extras/odbc` directory in the Mercury
Source Distribution. The biggest difference is, that the new one is thread-safe. You can run multiple threads with
database access.

## Requirements

This new ODBC library is being developed and tested on Linux. It will probably work on other platforms (such as
Windows) too, but I can't test that. Maybe some small adjustments would be necessary.

Apart from Linux, you need a working, recent version of the Mercury compiler. And you need to install and configure
UnixODBC on your system.

## Installing and setting up UnixODBC

You need to install UnixODBC for your Linux distribution. On Ubuntu, this are the packages `unixodbc` and
`unixodbc-dev`. If you don't already have a database, which you want to connect, then you need to install a
database server, such as MySQL or MariaDB. 

For MySQL/MariaDB, the following snipped can be use to create a database and set the access rights. Replace the
`<<database>>`, `<<user>>`, `<<host>>` and `<<password>>` parts with your respective values:

~~~
CREATE DATABASE <<database>> CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER '<<user>>'@'<<host>>' IDENTIFIED BY '<<password>>';
GRANT ALL ON <<database>>.* TO '<<user>>'@'<<host>>';
FLUSH PRIVILEGES;
~~~

Then you need to install an ODBC driver for your database management system. For MariaDB on Ubuntu, the package is
`odbc-mariadb`. 

The configuration files reside at the following places:

| Configuration file | Purpose |
| --- | --- |
| /etc/odbcinst.ini | Lists the installed ODBC drivers |
| /etc/odbc.ini	| Configures the system wide data sources |
| ~/.odbc.ini | Configures the user data sources |

Then you need to configure the ODBC driver and an ODBC data source. This involves calling the program `odbcinst`.
See here:

* For MariaDB: 
  * [Creating a Data Source with MariaDB Connector/ODBC on Linux](https://mariadb.com/kb/en/creating-a-data-source-with-mariadb-connectorodbc/#creating-a-data-source-with-mariadb-connectorodbc-on-linux) for MariaDB,
  * [Configuring a DSN with UnixODBC on Linux](https://mariadb.com/kb/en/creating-a-data-source-with-mariadb-connectorodbc/#configuring-a-dsn-with-unixodbc-on-linux)
* For MySQL:
  * [Configuring a Connector/ODBC DSN on Unix](https://dev.mysql.com/doc/connector-odbc/en/connector-odbc-configuration-dsn-unix.html) for MySQL.

You can test your configuration with the `isql` program, which is part of UnixODBC. Call it like this:

~~~
isql <<datasource>> <<user>> <<password>>
~~~

## Documentation

The documentation is in the comments in the interface section of the `odbc.m` file.

## Installation

This ODBC library doesn't require installation. It's just the `odbc.m` file and can be incorporated in your
project directly. However, you can install it with the `install.sh` script.

You can build the test program with the `mmc-make` script. It also shows which command line options for the
compiler are needed, when you build a program that uses the library.

## Copyright and Warranty

Copyright (C) 1997 Mission Critical.<br>
Copyright (C) 1997-2000, 2002, 2004-2006, 2010 The University of Melbourne.<br>
Copyright (C) 2017-2018, 2020, 2023 The Mercury team.<br>
Copyright (C) 2023-2024 Volker Wysk

The ODBC library is distributed under the terms of the GNU Library General Public License, Version 2 or any later
version. See the file COPYING.LIB for details.

It is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for
more details.
