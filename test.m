% This is a test and example program for the ODBC library.

:- module test.
:- interface.

:- import_module io.

:- pred main(io::di, io::uo) is cc_multi.


:- implementation.

:- import_module odbc.

:- import_module exception.
:- import_module list.
:- import_module pair.
:- import_module string.
:- import_module univ.
:- import_module unit.
:- import_module require.
:- import_module bool.
:- import_module maybe.
:- import_module int, int64, uint64.


:- pred print_data_sources(io::di, io::uo) is cc_multi.

:- pred handle_exceptions(
    pred(io, io)::pred(di, uo) is cc_multi,
    io::di, io::uo
) is cc_multi.


:- pragma no_determinism_warning(pred(main/2)).

main(!IO) :-
    print_data_sources(!IO),
    handle_exceptions(
        (pred(!.IO1::di, !:IO1::uo) is cc_multi :-
            io.write_string("Here comes a conv_error:\n", !IO1),
            Uint64 = 0xFFFFFFFFFFFFFFFFu64,
            Int = conv(Uint64),
            io.format("too big = %i\n", [i(Int)], !IO1)
        ),
        !IO),

    handle_exceptions(with_connection("odbctest", "odbctest", "odbctest", test_til, _), !IO),
    handle_exceptions(with_connection("odbctest", "odbctest", "odbctest", test_types, _), !IO),
    handle_exceptions(with_connection("odbctest", "odbctest", "odbctest", query_tables, _), !IO),
    handle_exceptions(with_connection("odbctest", "odbctest", "odbctest", test_es, _), !IO),
    handle_exceptions(with_connection("odbctest", "odbctest", "odbctest", test_conv, _), !IO),
    handle_exceptions(with_connection("odbctest", "odbctest", "odbctest", test_seq, _), !IO),
    handle_exceptions(with_connection("odbctest", "odbctest", "odbctest", test_seq2, _), !IO).



handle_exceptions(Pred, !IO) :-
    (
        try [io(!IO)] (
            Pred(!IO)
        )
        then true
        catch DbError : db_error ->
            io.write_string("DbError: ", !IO),
            io.write_string(db_error_message(DbError), !IO),
            io.nl(!IO)
        catch 0 -> true
    ).



:- pred test_til(connection::in, unit::out, io::di, io::uo) is cc_multi.

:- pragma no_determinism_warning(pred(test_til/4)).
    
test_til(Conn, unit, !IO) :-
    odbc.get_transaction_isolation_levels(Conn, Result1, !IO),
    io.write_string("\nGetting the supported transaction isolation levels:\nResult = ", !IO),
    io.print(Result1, !IO),
    io.nl(!IO),

    odbc.set_transaction_isolation_level(Conn, read_committed, Result2, !IO),    
    io.write_string("\nSetting the transaction isolation level:\nResult = ", !IO),
    io.print(Result2, !IO),
    io.nl(!IO).    



:- type myrecord
    ---> myrecord(string, int, int, bool).


:- pred test_types(connection::in, unit::out, io::di, io::uo) is cc_multi.

test_types(Conn, unit, !IO) :-
    io.write_string("\nGetting a row with many types:\n", !IO),
    transaction(
        Conn,
        (pred(Res::out, !.DB::di, !:DB::uo) is cc_multi :-
            odbc.solutions(
                "select * from fis_types",
                Res0,
                !DB),
            get_result(Res0, Res)),
        Result0,
        !IO),

    get_result(Result0, Result),
    io.write_string("Result=\n   ", !IO),
    io.write_list(Result, "\n   ", io.write, !IO),
    io.nl(!IO).




:- pred test_es(connection::in, unit::out, io::di, io::uo) is cc_multi.

test_es(Connection, unit, !IO) :-
    io.write_string("\nDoing something on the fis_path table:\n", !IO),
    transaction(
        Connection,
        (pred({RowCount1, Records1}::out, !.DB::di, !:DB::uo) is cc_multi :-
            odbc.execute(
                "insert into fis_path values (""neuneuneu"", 123, 456, 0)",
                ExecuteResult,
                !DB),
            get_result(ExecuteResult, RowCount1),

            odbc.solutions(
                "select * from fis_path",
                Res,
                !DB),
            rows(
                Res,
                (func([string(Path), uint64(Dev), uint64(Ino), int8(IsDir)]) =
                    myrecord(Path, conv(Dev), conv(Ino), conv(IsDir)) is semidet),
                Records1)
            %rollback("Canceled!", !DB)
        ),
        Result0,
        !IO),
    get_result(Result0, {RowCount, Records}),

    io.format("RowCount=%i\n", [i(RowCount)], !IO),
    io.write_string("Records=\n   ", !IO),
    io.write_list(Records, "\n   ", io.write, !IO),
    io.nl(!IO).




:- pred query_tables(connection::in, unit::out, io::di, io::uo) is cc_multi.

query_tables(Connection, unit, !IO) :-
    io.write_string("\nQuerying the tables of the odbctest database:\n   ", !IO),
    transaction(
        Connection,
        (pred(Result2::out, !.DB::di, !:DB::uo) is cc_multi :-
            odbc.tables("odbctest", "", "", Result1, !DB),
            get_result(Result1, Result2)
        ),
        Result0,
        !IO),

    get_result(Result0, Result),
    io.write_string("Result=\n   ", !IO),
    io.write_list(Result, "\n   ", io.write, !IO),
    io.nl(!IO).


print_data_sources(!IO) :-
    io.write_string("Data sources on the system:\n", !IO),
    with_environment(
        (pred(Environment::in, unit::out, !.IO1::di, !:IO1::uo) is cc_multi :-
            data_sources(Environment, Result, !IO1),
            get_result(Result, Res),
            io.write_list(Res, "\n", io.write, !IO1),
            io.nl(!IO1)
        ),
        _,
        !IO),
    io.write_string("\n", !IO).



:- pred test_conv(connection::in, unit::out, io::di, io::uo) is cc_multi.

test_conv(Connection, unit, !IO) :-
    io.write_string("\nConverting attributes which can be null:\n", !IO),
    transaction(
        Connection,
        (pred(Res1::out, !.DB::di, !:DB::uo) is cc_multi :-
            odbc.solutions(
                "select * from fis_conv",
                Res,
                !DB),
            rows(
                Res,
                (func([MaybeInt, MaybeTimeStamp, MaybeFloat, MaybeInt64]) =
                    { conv(MaybeInt), conv(MaybeTimeStamp), conv(MaybeFloat), conv(MaybeInt64) } is semidet),
                Res1)
        ),
        Result0,
        !IO),
    get_result(Result0, Result : list({ maybe(int), maybe(string), maybe(float), maybe(int64) })),

    io.write_string("Result=\n   ", !IO),
    io.write_list(Result, "\n   ", io.write, !IO),
    io.nl(!IO).


:- pred test_seq(connection::in, unit::out, io::di, io::uo) is cc_multi.

test_seq(Connection, unit, !IO) :-
    io.write_string("\nRetreiving only one row from the result set:\n", !IO),
    test_seq_1(Connection, "select * from fis_path", !IO),

    io.write_string("Causing an error when retreiving only one row from the result set:\n", !IO),
    test_seq_1(Connection, "<<invalid>>", !IO).



:- pred test_seq_1(connection::in, string::in, io::di, io::uo) is cc_multi.

test_seq_1(Connection, SQL, !IO) :-
    transaction(
        Connection,
        (pred({Res1, Res2, Res3}::out, !.DB::di, !:DB::uo) is cc_multi :-
            some [!Statement] (
                seq_begin(SQL, !:Statement, Res1, !DB),
                seq_get(Res2, !Statement, !DB),
                seq_end(Res3, !.Statement, !DB)
            )
        ),
        Result,
        !IO),

    get_result(Result, {Result1, Result2, Result3}), 

    io.format("   Result1=%s\n", [s(string(Result1))], !IO),
    io.format("   Result2=%s\n", [s(string(Result2))], !IO),
    io.format("   Result3=%s\n", [s(string(Result3))], !IO),
    io.nl(!IO).



:- pred test_seq2(connection::in, unit::out, io::di, io::uo) is cc_multi.

test_seq2(Connection, unit, !IO) :-
    io.write_string("Getting something with with_connection:\n", !IO),
    test_seq2_1(Connection, "select st_dev from fis_path where path = ""neuneuneu""", !IO),

    io.write_string("\nFailing with with_connection:\n", !IO),
    test_seq2_1(Connection, "select st_dev from fis_path where path = ""xxx""", !IO).


:- pred test_seq2_1(connection::in, string::in, io::di, io::uo) is cc_multi.

test_seq2_1(Connection, SQL, !IO) :-
    transaction(
        Connection,
        (pred(Res::out, !.DB::di, !:DB::uo) is cc_multi :-
            with_statement(
                SQL,
                seq_get,
                Res1,
                Res23,
                !DB
            ),
            Res = { Res1, Res23 }
        ),
        Result, 
        !IO),
    get_result(Result, { Result1, Result23 }),
    io.write_string("Result1  = " ++ string.string(Result1) ++ "\n", !IO),
    io.write_string("Result23 = " ++ string.string(Result23) ++ "\n", !IO).
