%====================================================================================================
% Mercury ODBC Library, Version 1.5.0
%====================================================================================================
% vim: ft=mercury ts=4 sw=4 et
%---------------------------------------------------------------------------%
% Copyright (C) 1997 Mission Critical.
% Copyright (C) 1997-2000, 2002, 2004-2006, 2010 The University of Melbourne.
% Copyright (C) 2017-2018, 2020, 2023 The Mercury team.
% Copyright (C) 2023-2024 Volker Wysk
% This file is distributed under the terms specified in COPYING.LIB.
%---------------------------------------------------------------------------%
%
% File: odbc.m.
% Authors: Renaud Paquay (rpa@miscrit.be), stayl,
%          Volker Wysk <post@volker-wysk.de>
% ODBC version: 3 (It's whatever 3.x version UnixODBC supports.)
%
% The transaction interface used here is described in the following paper:
%
%   Kemp, Conway, Harris, Henderson, Ramamohanarao and Somogyi,
%   "Database transactions in a purely declarative logic programming language".
%   In Proceedings of the Fifth International Conference on Database
%   Systems for Advanced Applications, pp. 283-292.
%   Melbourne, Australia, 1-4 April, 1997.
%
%   An earlier(?) version of this paper is available as
%   Technical Report 96/45, Department of Computer Science,
%   University of Melbourne, December 1996,
%   <https://mercurylang.org/documentation/papers/tr_96_45_cover.ps.gz>
%   and <https://mercurylang.org/documentation/papers/tr_96_45.ps.gz>.
%
% This has been developed and tested on Linux and MySQL/MariaDB only. It
% should also run on other platforms, but there's no guarantee. Mercury's ODBC
% "extra" library has been developed on a wider spectrum of operating systems
% and database management systems. It can be found in the "extras/odbc"
% directory in the Mercury source distribution.
%
% Notes:
%
%   Binary data is converted to a string of hexadecimal digits.
%
%   This module requires a compilation grade with conservative garbage
%   collection. Any grade containing .gc in its name, such as asm_fast.gc,
%   will do. See the section "Compilation model options" in the Mercury
%   User's Guide for more information.
%
%   The header files distributed with the Microsoft ODBC SDK require
%   some modification for compilation with gcc. In particular,
%   some conflicting typedefs for SQLUINTEGER and SQLSCHAR must be
%   removed from sqltypes.h.
%   (For legal reasons a patch cannot be included in the Mercury
%   distribution.)
%
%   This version of an ODBC library is a big revamp of the (quite old) Mercury
%   "extra" ODBC library. Contrary to the "extra" library, it is thread-safe.
%   You can run multiple instances in separate threads.
%
% To do:
%
%   Improve the interface to the catalog functions.
%
%   Add a nicer interface so the user does not need to manipulate
%   SQL strings.
%
%   Support for prepared queries.
%
%   Add a blob type.
%
%-----------------------------------------------------------------------------%

:- module odbc.
:- interface.

% Contents:
%
% 1. Results
% 2. Extracting information from a query result
% 3. Conversion of attribute values
% 4. Environments and Connections
% 5. Transactions
% 6. Queries
% 7. Catalog functions
% 8. Error handling
% 9. Helpers for building SQL queries

:- import_module io, pair, list, string, maybe, bool, univ.
:- import_module int, int64, uint64, int32, uint32, int16, uint16, int8, uint8.


%----------------------------------------------------------------------------------------------------
% 1. Results
%----------------------------------------------------------------------------------------------------

% The first one of the two "result" types, which follow below, is for operations which return a value. The second
% one is for operations which don't. In both cases, there can be ODBC warning and error messages.
%
% In case of success, "ok(Val, Warnings)" or "ok(Warnings)", respectively, is returned. The Warnings are the list
% of the ODBC warning messages, which occured, in the order in which they occured. When the warning list is empty,
% all involved ODBC actions were successful (SQL_SUCCESS). If the it isn't, then there were some ODBC actions which
% returned warnings (SQL_SUCCESS_WITH_INFO).
%
% In case of an error, there's no value returned, and the result is "error(Messages)". The messages can be both
% warning and error messages, but there is at least one error message. The messages are in the order, in which they
% occured.
%
% See also the warnings/1 and errors/1 functions. They extract the warnings or errors, respectively, from a list of
% messages.

:- type odbc.result(T)
    ---> odbc.ok(T, odbc.warnings)
    ;    odbc.error(odbc.messages).

:- type odbc.result
    ---> odbc.ok(odbc.warnings)
    ;    odbc.error(odbc.messages).


% The following collects all the exceptions which can be thrown by the ODBC library.
%
% The first one, "odbc_error", means that a database error occured (when an ODBC call returned something different
% from SQL_SUCCESS and SQL_SUCCESS_WITH_INFO). The carried messages list includes both warning and errors messages.
% See also the warnings/1 and errors/1 functions. They extract the warnings or errors, respectively, from a list of
% messages.
%
% The second and third ones, "match_error" and "conv_error", occur when there's an inconsistency between what is
% returned by solutions/4 or tables/6 and what was expected by the program. The types don't match.
%
% A match_error is thrown by the "row" and "row_..." predicates, when the semidet conversion function fails. It
% carries the first row from the database, which didn't match.
%
% A "conv_error" is thrown, when a call to the function conv/1 fails. This means that the value can't be converted
% to the intended to the target type. It carries the names of theFrom and To types and a univ with the value that
% couldn't be converted.
%
% A "rows_error(Which, Rows)" is thrown by the "rows_..." predicates and informs the caller that the number of rows
% received from the database doesn't match the expectations. The "Which" argument has the enumeration type
% "rows_error" and informs, which of the "rows_..." predicates failed. The "Rows" argument is the list of the
% actually received (non-converted) rows from the database.

:- type odbc.db_error
    ---> odbc.odbc_error(odbc.messages)
    ;    odbc.match_error(odbc.row)
    ;    odbc.conv_error(string, string, univ)
    ;    odbc.rows_error(odbc.rows_error, list(odbc.row)).


% This enumeration tells you which of the "rows_..." predicates has thrown a rows_error (see above).

:- type rows_error
    ---> is_not_one             % Thrown by rows_one
    ;    is_not_at_most_one     % Thrown by rows_at_most_one
    ;    is_not_at_least_one.          % Thrown by rows_multi



% This makes an error message from the specified db_error.

:- func db_error_message(db_error) = string.


%----------------------------------------------------------------------------------------------------

% Unwrap the result value which is wrapped in an odbc.result/1 value. If the value indicates success (the odbc.ok/2
% data constructor), the wrapped value is returned. If it isn't, an odbc_error/1 exception is thrown. The warning
% messages must be taken care of explicitly, they aren't returned by get_result/2.

:- pred get_result(odbc.result(T)::in, T::out) is det.


% The following two predicates ensure that a result isn't an error. They throw a odbc_error exception otherwise.
% This is for the odbc.result/0 type only, which doesn't carry a wrapped return value. For the odbc.result/1 type,
% which wraps a return value, get_result/2 and the row/row_... predicates should be used instead.
%
% The throw_odbc_error predicates work as intended in the default strict commutative semantics. They may not work
% as layed out here, in other semantics.
%
% The two versions of throw_odbc_error below, differ in how they cope with compiler conduct. The compiler may
% optimize away such calls, when no countermeasures are taken. It also may change the order of such calls, as long
% as the dependencies on the arguments don't determine the order in the compiled program.
%
% The first throw_odbc_error predicate depends on the IO state (although it doesn't do anything with it). The
% dependency on the IO state determines the order of the throw_odbc_error/3 call with respect to other IO actions.
% The compiler assigns this order. This means that the compiler won't change the order of IO actions. The
% compiler may still change the order of the throw_odbc_error/3 call with respect to non-IO-actions, as long as the
% order isn't determined by the dependencies on the first argument (the result value to inspect) of the call.
%
% The second one (throw_odbc_error/2) returns a copy of the input, which has undergone the check of being an error.
% This copy needs to be used, in place of the input, otherwise the call to throw_odbc_error might get optimized
% away.
%
% See the chapter "Exception Handling" in the Mercury Language Reference Manual for more information.

:- pred throw_odbc_error(
    odbc.result::in,                            % Result to examine, such as returned by odbc.execute/4.
    io::di, io::uo
) is det.

:- pred throw_odbc_error(
    odbc.result::in,                            % Result to examine, such as returned by odbc.execute/4.
    odbc.result::out(bound(odbc.ok(ground)))    % Copy of the result, which isn't an odbc.error/1.
) is det.



%----------------------------------------------------------------------------------------------------
% 2. Extracting information from a result
%----------------------------------------------------------------------------------------------------


% The rows_one/3 predicate detected that there isn't exactly one result. This includes the list of the converted
% rows, which is empty or contains more than one element.
:- type is_not_one(T)
    ---> is_not_one(list(T)).


% The rows_at_most_one/3 predicate detected that there are several results. This includes the list of the converted
% rows.
:- type is_not_at_most_one(T)
    ---> is_not_at_most_one(list(T)).


% The rows_multi/3 predicate detected that there is no result.
:- type is_not_at_least_one
    ---> is_not_at_least_one.


% Convert a list of rows (such as returned by the odbc.solutions predicate). This makes use of the specified
% semidet conversion function. When it fails for a row, then a match_error/1 exception, is thrown, which includes
% the unexpected row. The output of this predicate is the list of the converted rows.

:- pred rows(
    odbc.result(list(odbc.row))::in,
    (func(odbc.row) = T)::in(func(in) = out is semidet),
    list(T)::out
) is det.


% Convert a list of rows (such as returned by the odbc.solutions predicate). This makes use of the specified
% semidet conversion function. When it fails for a row, then a match_error/1 exception, is thrown, which includes
% the unexpected row.
%
% Additionally, the row list is tested for consisting of one argument. If it doesn't, a rows_error(is_not_one, L)
% exception is thrown. The argument L are the rows which have been received (see the rows_error data constructor of
% the odbc_error exception).

:- pred rows_one(
    odbc.result(list(odbc.row))::in,
    (func(odbc.row) = T)::in(func(in) = out is semidet),
    T::out
) is det.


% Convert a list of rows (such as returned by the odbc.solutions predicate). This makes use of the specified
% semidet conversion function. When it fails for a row, then a match_error/1 exception, is thrown, which includes
% the unexpected row.
%
% Additionally, the row list is tested for consisting of zero or one arguments. If it doesn't, a
% rows_error(is_not_at_most_one, L) exception is thrown. The argument L are the rows which have been received (see
% the rows_error data constructor of the odbc_error exception).
%
% The output of this predicate is of type maybe(T). When there is one element, then a "yes(Element)" is returned.
% Else, a "no" is returned.

:- pred rows_at_most_one(
    odbc.result(list(odbc.row))::in,
    (func(odbc.row) = T)::in(func(in) = out is semidet),
    maybe(T)::out
) is det.


% Convert a list of rows (such as returned by the odbc.solutions predicate). This makes use of the specified
% semidet conversion function. When it fails for a row, then a match_error/1 exception, is thrown, which includes
% the unexpected row.
%
% Additionally, the result list is tested for consisting of at least one element. If it is empty, a
% rows_error(is_not_at_least_one, []) exception is thrown.
%
% The output of this predicate is the list of the converted rows (with the inst of non_empty_list).

:- pred rows_at_least_one(
    odbc.result(list(odbc.row))::in,
    (func(odbc.row) = T)::in(func(in) = out is semidet),
    list(T)::out(non_empty_list)
) is det.



%----------------------------------------------------------------------------------------------------
% 3. Conversion of attribute values
%----------------------------------------------------------------------------------------------------

% Here are some type conversion functions. They are handy for converting the types that come from the database (via
% odbc.solutions/5) into something needed by the application. This ODBC library returns values of database integer
% types as the exact counterpart in the Mercury program. This means that they have one of the following Mercury
% types: int64, uint64, int32, uint32, int16, uint16, int8 and uint8. An "int" isn't returned.
%
% Most of the time, you will want to convert the database side integer type to Mercury side integer of type int.
% This can be done with the conversion functions, which follow.
%
% Mercury's int type can theoretically be either 64 bits or 32 bits wide. In reality, it's 64 bits. The conversion
% functions assume that "int" is a 64 bit signed integer. This means, you can always convert the integers, which
% come from the database, to "int", except for the type "uint64". This one has the range 0 to 2^64-1, whereas the
% "int" type goes from -2^63 to 2^63-1. When it can't be converted to "int", then conv function throws a
% conv_error exception.
%
% The conv/1 function is meant to be used in the conversion function argument of the "rows" and "rows_..."
% predicates.
%
% You can also convert attributes, which may be NULL, to values of type "maybe(T)", where T is a supported type.
% "no" means a NULL value for the attribute, "yes(Val)" means it is non-NULL. When you do it like this, you convert
% a value of type odbc.attribute.
%
% When your attribute can't be NULL, then you have the choice of what you convert. You can either convert the value
% which is wrapped inside one of the data constructors of type odbc.attribute, or you convert the value of type
% odbc.attribute itself.
%
% Example:
%
% rows(
%     Result,
%     (func([ uint64(First), Second ]) =
%           { conv(First) : int, conv(Second) : maybe(string) }
%      is semidet),
%     Result1)
%
% Here you have an attribute of type uint64 (First), which will be converted to an int. It can't be NULL. When you
% still get a NULL for this attribute, it results in a "conv_error/3" exception. And you have a second attribute
% (Second), that can be NULL. This is converted from something to a string - such as a VARCHAR(...) or a TIMESTAMP.
% Since this attribute can be NULL, it will be converted to a "maybe(string)".

:- typeclass conv(From, To) where [
    func conv(From) = To
].

% Integer types
:- instance conv(int64,  int).
:- instance conv(uint64, int).
:- instance conv(int32,  int).
:- instance conv(uint32, int).
:- instance conv(int16,  int).
:- instance conv(uint16, int).
:- instance conv(int8,   int).
:- instance conv(uint8,  int).
:- instance conv(int8,   bool).

% String to int conversion. This is for convenience, it isn't necessary.
:- instance conv(string, int).

% These two are needed when you convert an attribute that can be NULL.
:- instance conv(string, string).
:- instance conv(float, float).

% These convert the attributes, not the values wrapped inside the attribute data constructors. They are for
% attributes which can't be NULL and are used by the conversion functions for attributes that can.
:- instance conv(attribute, int).
:- instance conv(attribute, string).
:- instance conv(attribute, float).

% These are for when you want the explicit integer types from the database. They are involved when you convert
% attributes which are allowed to be NULL.
:- instance conv(attribute, int64).
:- instance conv(attribute, uint64).
:- instance conv(attribute, int32).
:- instance conv(attribute, uint32).
:- instance conv(attribute, int16).
:- instance conv(attribute, uint16).
:- instance conv(attribute, int8).
:- instance conv(attribute, uint8).

% This makes conversion functions for attributes that can be NULL.
:- instance conv(attribute, maybe(T)) <= conv(attribute, T).


% This exception is thrown, when the conversion of a value with the conv/1 function fails. The first two arguments
% are the names of the type (such as "int32") from which to convert, and the type to which to convert. The third
% argument is the value which failed to convert form, wrapped with in a univ type.




%----------------------------------------------------------------------------------------------------
% 4. Environments and Connections
%----------------------------------------------------------------------------------------------------

% The predicates which follow, can be used to manually open and close ODBC environments and connections. But
% normally one would just use with_connection/8 (following).


% Allocate an ODBC environment and an ODBC connection, which is based on this environment. Then do something with
% the connection. Afterwards, deallocate both. The fifth argument of with_connection/8 is the return value of the
% action, which uses the connection. Errors when opening or closing the environment, are thrown as odbc_error/1
% exceptions.

:- pred with_connection(
    string::in,        % Data source
    string::in,        % User (can be empty)
    string::in,        % Password (can be empty)

    % What to do with the connection. This returns a result value, which will be passed to the caller in the
    % following argument.
    pred(connection, T, io, io)::pred(in, out, di, uo) is cc_multi,

    T::out,            % Return value of the action.
    io::di, io::uo
) is cc_multi.


% An ODBC environment handle.
:- type environment.


% An ODBC connection handle (also contains the environment handle).
:- type connection.


% Make a new ODBC environment.
:- pred open_environment(
    odbc.result(environment)::out,      % New ODBC environment
    io::di, io::uo
) is det.


% Close an ODBC environment.
:- pred close_environment(
    environment::in,                    % ODBC environment
    odbc.result::out,                   % Result
    io::di, io::uo
) is det.


% Open an ODBC connection in the specified environment.
:- pred open_connection(
    environment::in,                    % ODBC environment
    string::in,                         % Data source
    string::in,                         % User (can be empty)
    string::in,                         % Password (can be empty)
    odbc.result(connection)::out,       % New database connection. This holds the ODBC environment and connection
                                        % handles.
    io::di, io::uo
) is det.


% Close an ODBC connection.
:- pred close_connection(
    connection::in,                     % ODBC database connection
    odbc.result::out,
    io::di, io::uo
) is det.


% Allocate an ODBC environment and do something with it. Afterwards, deallocate it. The second argument of
% with_environment/4 is the return value of the action, which uses the environment. Errors opening or closing the
% environment, are thrown as odbc_error exceptions.

:- pred with_environment(
    pred(environment, T, io, io)::pred(in, out, di, uo) is cc_multi, % What to do with the environment. (Action)
    T::out,                                                     % Return value of the action.
    io::di, io::uo
) is cc_multi.


% Allocate an ODBC connection and do something with it. This needs the ODBC environment handle. For a version of
% with_connection, which doesn't, see above. After the call of the specified action, the connection is closed.
% The sixth argument of with_connection/8 is the return value of the action, which uses the connection. Errors
% opening or closing the connection, are thrown as odbc_error exceptions.

:- pred with_connection(
    environment::in,                    % ODBC environment in which to open the connection.
    string::in,                         % Data source
    string::in,                         % User (can be empty)
    string::in,                         % Password (can be empty)

    % What to do with the connection. This returns a result value, which will be passed to the caller in the
    % following argument.
    pred(connection, T, io, io)::pred(in, out, di, uo) is cc_multi,

    T::out,                             % Return value of the action.
    io::di, io::uo
) is cc_multi.


%----------------------------------------------------------------------------------------------------
% 5. Transactions
%----------------------------------------------------------------------------------------------------

:- type odbc.state.


% The main transaction predicate. This is called when something is to be done with the database. It executes its
% second argument inside a database transaction, commits (or rolls back) it and returns the output value to the
% caller. The database state is created and passed to the transaction to be run. With the help of this state,
% database operations are accomplished.

:- pred odbc.transaction(
    connection::in,                     % Database connection
    pred(T,                             % Output value to be returned when the transaction has been finished
                                        % successfully.
         odbc.state, odbc.state         % Database state
    )::in(pred(out, di, uo) is cc_multi),
    odbc.result(T)::out,                % Wrapped result or error
    io::di, io::uo
) is cc_multi.


% This can be called by the user inside of a transaction. It causes the running transaction to be rolled back and
% aborted. The control flow ends where rollback/3 is called and is resumed after the call of transaction/5. (An
% exception is used for this.) The result of the transaction/5 call will be an odbc.error/1, which carries a
% message list. The first element of this list will be "error(user_requested_rollback) - Msg", with "Msg" being the
% message which has been supplied in the rollback/3 call. There may be additional messages after that. When there
% are any, then they occured in the rollback operation, in the SQLEndTran() ODBC call.

:- pred rollback(
    string::in,                         % Message to include in the transaction/5 result.
    odbc.state::di, odbc.state::uo      % Database state
) is erroneous.


% The following structure is returned by get_transaction_isolation_levels/4. It states which transaction isolation
% levels which supported by the DBMS and the ODBC driver.

:- type transaction_isolation_levels
    ---> transaction_isolation_levels(
        read_uncommitted :: bool,       % SQL_TXN_READ_UNCOMMITTED
        read_committed   :: bool,       % SQL_TXN_READ_COMMITTED
        repeatable_read  :: bool,       % SQL_TXN_REPEATABLE_READ
        serializable     :: bool        % SQL_TXN_SERIALIZABLE
    ).


% Query the available transaction isolation levels. The result contains a bool value for each transaction
% isolation level. It's yes for the supported ones.

:- pred get_transaction_isolation_levels(
    connection::in,                                     % Database connection
    odbc.result(transaction_isolation_levels)::out,     % Wrapped result or error
    io::di, io::uo
) is det.


% This enumerates all the transaction isolation levels.

:- type transaction_isolation_level
    ---> read_uncommitted               % SQL_TXN_READ_UNCOMMITTED
    ;    read_committed                 % SQL_TXN_READ_COMMITTED
    ;    repeatable_read                % SQL_TXN_REPEATABLE_READ
    ;    serializable.                  % SQL_TXN_SERIALIZABLE


% Set the transaction isolation level for the given connection.

:- pred set_transaction_isolation_level(
    connection::in,                     % Database connection
    transaction_isolation_level::in,    % Transaction isolation level to set
    odbc.result::out,                   % ok or error(_,_)
    io::di, io::uo
) is det.



%----------------------------------------------------------------------------------------------------
% 6. Queries
%----------------------------------------------------------------------------------------------------

% This is about database queries. execute/4 executes a database query (such as INSERT or DELETE), which
% doesn't return a result set. And solutions/5 is for SELECT queries, which do return a result set.


% A row in the result set, returned by solutions/5. This is a list of database attributes.

:- type row == list(odbc.attribute).


% An attribute in a row, like returned by solutions/5 and tables/6.

:- type odbc.attribute
    --->    null                % SQL NULL value
    ;       int64(int64)
    ;       uint64(uint64)
    ;       int32(int32)
    ;       uint32(uint32)
    ;       int16(int16)
    ;       uint16(uint16)
    ;       int8(int8)
    ;       uint8(uint8)
    ;       string(string)
    ;       float(float)
    ;       time(string).       % Time string: "YYYY-MM-DD hh:mm:ss.mmm"


% Execute an SQL query which doesn't return any results, such as INSERT or DELETE. The result is the row count -
% the number of affected rows.

:- pred execute(
    string::in,                         % SQL statement
    odbc.result(int)::out,              % Row count or error
    odbc.state::di, odbc.state::uo
) is det.


% Execute a SELECT SQL query which returns a result set. If the query succeeds, then the entire result set is
% returned. It is a list of rows, which in turn are lists of attributes.

:- pred solutions(
    string::in,                         % SQL statement
    odbc.result(list(odbc.row))::out,   % Result (rows)
    odbc.state::di, odbc.state::uo
) is det.


%----------------------------------------------------------------------------------------------------

% Execute a SELECT SQL query which returns a result set, but don't get it all at once. Instead, return it
% sequentially with seq_get. It returns one result row per call, until all the rows have been returned. You don't
% have to read the entire result set this way. You can end the process at any time by calling seq_end. This can be
% faster than odbc.solutions/4, if not all results are needed.

% A statement handle. Used by seq_get/5 and seq_end/4.

:- type statement.


% Execute an SQL query and prepare the return of the result rows. This returns the statement handle, which is
% used by subsequent calls to seq_get/5 and seq_end/4. In case of an error, it is invalid (a null pointer) and the
% odbc.result is set accordingly.
%
% (This is not optimal. The predicate should return the statement handle in an odbc.ok/2, and not return an
% invalid statement in case of an error. But this can't be done, because the MMC implementation of unique modes
% isn't complete. See section "6.3 Limitations of the current implementation" in the Language Reference Manual.)

:- pred seq_begin(
    string::in,                                 % SQL SELECT query
    odbc.statement::uo,                         % Statement
    odbc.result::out,                           % Information about errors. See odbc.result/0.
    odbc.state::di, odbc.state::uo              % Database state
) is det.


% Retrieve the next result row from the statement. In case there is another row, it returns "odbc.ok(yes(Row),
% Warnings)". In case there isn't, "odbc.ok(no, Warnings)" is returned. An error is returned as a
% "odbc.error(Messages)". When there are no more rows, the statement is closed. seq_end/4 doesn't need to be
% called in this case (but it doesn't hurt).

:- pred seq_get(
    odbc.result(maybe(odbc.row))::out,          % Result row
    odbc.statement::di, odbc.statement::uo,     % Statement state
    odbc.state::di, odbc.state::uo              % Database state
) is det.


% Terminate the retrieval of result rows from the statement. This frees any storage associated with the statement
% and closes the ODBC statement handle. This needs to be called only if seq_get/5 hasn't returned "no" yet. In
% case it has, this is a no-op.

:- pred seq_end(
    odbc.result::out,                           % Result - ok(Warnings) or error(Errors)
    odbc.statement::di,                         % Statement state
    odbc.state::di, odbc.state::uo              % Database state
) is det.


% with_statement(SQL, Pred, Result1, MaybeResult23, !DB)
% 
% Do something with a statement and clean it up afterwards. This calls seq_begin, and (if applicable) the action
% and seq_end, and returns all the results of these calls. If the call of seq_begin fails, then the action and
% seq_end aren't called. Then Result1 is an "odbc.error" and MaybeResult23 is a "maybe.no". If seq_begin succeeds,
% then MaybeResult23 is "maybe.yes({ Result2, Result3 })". Then the value of the action is in "Result2", if it
% succeeded. The result of seq_end is in Result3.
%
% If the action throws an exception, then the statement is cleaned up (with seq_end) and the exception is
% rethrown.

:- pred with_statement(
    string,                                     % (in) SQL SELECT query
    pred(                                       % (in) Predicate that is called with the statement
        odbc.result(T),                         % The result of the predicate
        odbc.statement, odbc.statement,         % Statement handle
        odbc.state, odbc.state                  % Database state
    ),
    odbc.result,                                % (out) The result of seq_begin
    maybe(                                      % (out) If seq_begin was successful, then
        { odbc.result(T),                       % The result of the predicate
          odbc.result                           % The result of seq_end
        }
    ),
    odbc.state, odbc.state                      % (di, uo) Database state
).

:- mode with_statement(
    in, in(pred(out, di, uo, di, uo) is det),
    out, out,
    di, uo
) is cc_multi.

:- mode with_statement(
    in, in(pred(out, di, uo, di, uo) is cc_multi),
    out, out,
    di, uo
) is cc_multi.




%----------------------------------------------------------------------------------------------------
% 7. Catalog functions
%----------------------------------------------------------------------------------------------------

% The source_desc type includes the description of the DRIVER of the data source, not the description of the data
% source (like specified in the "Description=..." field in .odbc.ini). This is in accordance with the ODBC
% standard. See:
%
% https://learn.microsoft.com/en-us/sql/odbc/reference/syntax/sqldatasources-function?view=sql-server-ver16

:- type source_desc
    --->    source_desc(
                string,   % Name
                string    % Description of the DRIVER
            ).


% This returns a list of all configured data sources (as specified in .odbc.ini). You need an ODBC environment, see
% with_environment/4.

:- pred data_sources(
    environment::in,                            % ODBC environment
    odbc.result(list(odbc.source_desc))::out,   % The list of the descriptions of all configured data sources.
    io::di, io::uo
) is det.


%----------------------------------------------------------------------------------------------------

% Query information about the tables in one or more databases. You need the appropriate permissions in order to
% list the tables of a database.
%
% A data source might return more information than the first five strings, that describe a table. In this case, the
% extra information (extra columns in the result set) are returned as the sixth field of the following data type.

:- type table_desc
    ---> table_desc(
             table_catalog_name :: string,                 % Table qualifier (catalog/database name)
             table_owner        :: string,                 % Table owner
             table_name         :: string,                 % Table name
             table_type         :: string,                 % Table type
             table_description  :: string,                 % Description
             table_extra        :: list(odbc.attribute)    % Data source specific extra information
         ).


% Get a list of database tables matching the given properties: catalog (database) name, owner and table name. These
% three are either the empty string or a pattern like it is used in an SQL "LIKE" clause. A "%" inside such a
% pattern matches any substring. An empty string, and a string composed of a single "%", always match. See here for
% details:
%
% https://dev.mysql.com/doc/refman/8.0/en/string-comparison-functions.html
%
% This must be run inside a transaction.

:- pred tables(
    string::in,                                 % Catalog name pattern
    string::in,                                 % Owner pattern
    string::in,                                 % Table name pattern
    odbc.result(list(odbc.table_desc))::out,    % Result
    odbc.state::di, odbc.state::uo
) is det.



%----------------------------------------------------------------------------------------------------
% 8. Error handling
%----------------------------------------------------------------------------------------------------

% Warning and error messages are returned inside the odbc.result/0 and odbc.result/1 types.


% An ODBC message. Can be a (further described) error or a warning.
:- type odbc.message  == pair(odbc.message_type, string).

% A list of messages
:- type odbc.messages == list(odbc.message).

% A list for warnings
:- type odbc.warnings == list(pair(odbc.warning, string)).

% List of errors
:- type odbc.errors   == list(pair(odbc.error, string)).

% Get all the warnings inside the list of messages and convert them to warnings.
:- func warnings(odbc.messages) = odbc.warnings.

% Get all the errors inside the list of messages and convert them to errors.
:- func errors(odbc.messages) = odbc.errors.

% Succeeds when the message is an error.
:- pred is_error(odbc.message::in) is semidet.

% Succeeds when the message is a warning.
:- pred is_warning(odbc.message::in) is semidet.


:- type odbc.message_type
    --->    warning(odbc.warning)
    ;       error(odbc.error).

:- type odbc.warning
    --->    disconnect_error
    ;       fractional_truncation
    ;       general_warning
    ;       null_value_in_set_function
    ;       privilege_not_revoked
    ;       privilege_not_granted
    ;       string_data_truncated.

:- type odbc.error
    --->    connection_error(odbc.connection_error)
    ;       execution_error(odbc.execution_error)
    ;       feature_not_implemented
    ;       general_error
    ;       internal_error
    ;       timeout_expired
    ;       transaction_error(odbc.transaction_error)
    ;       user_requested_rollback.

:- type odbc.connection_error
    --->    unable_to_establish
    ;       invalid_authorization
    ;       connection_name_in_use
    ;       nonexistent_connection
    ;       connection_rejected_by_server
    ;       connection_failure
    ;       timeout_expired
    ;       unknown_data_source.

:- type odbc.execution_error
    --->    column_already_exists
    ;       column_not_found
    ;       division_by_zero
    ;       general_error
    ;       incorrect_count_field
    ;       incorrect_derived_table_arity
    ;       index_already_exists
    ;       index_not_found
    ;       integrity_constraint_violation
    ;       interval_field_overflow
    ;       invalid_cast_specification
    ;       invalid_date_time
    ;       invalid_escape
    ;       invalid_insert_value_list
    ;       invalid_schema_name
    ;       invalid_use_of_default_parameter
    ;       length_mismatch_in_string_data
    ;       no_default_for_column
    ;       overflow
    ;       range_error
    ;       restricted_data_type_violation
    ;       string_data_length_mismatch
    ;       string_data_truncated
    ;       syntax_error_or_access_violation
    ;       table_or_view_already_exists
    ;       table_or_view_not_found.

:- type odbc.transaction_error
    --->    rolled_back
    ;       still_active
    ;       serialization_failure
    ;       invalid_state.



%----------------------------------------------------------------------------------------------------
% 9. Helpers for building SQL queries
%----------------------------------------------------------------------------------------------------

% Quote a string as a string literal, for use inside an SQL statement. The quoted string is enclosed in quote
% characters. Note: for a "LIKE" statement, you need sqlquote_like. The percent sign needs to be quoted, if you
% don't want to use it as a wildcard. On the other hand, the percent sign *must not* be quoted otherwise.

:- func sqlquote(string) = string.
:- func sqlquote_like(string) = string.


% Same as above, but the quoted string isn't enclosed in quote characters. Useful if you want to create a part of
% a quoted string.

:- func sqlquote0(string) = string.
:- func sqlquote_like0(string) = string.



%====================================================================================================

:- implementation.
:- import_module unit, exception, require, assoc_list, char.


% Since most (all?) function which do ODBC calls, call odbc_check afterwards, they all need to specify
% "may_call_mercury". And they all need to specify "thread_safe".

% SQL Data Types: see
% https://learn.microsoft.com/en-us/sql/odbc/reference/appendixes/sql-data-types?view=sql-server-ver16


% A note on SQLAllocHandle(): When allocating the handle fails, then the function returns a null pointer in its
% third argument. This means that the returned handle is invalid. This happens, for instance, if the necessary
% memory can't be allocated. See here:
%
% https://learn.microsoft.com/en-us/sql/odbc/reference/syntax/sqlallochandle-function?view=sql-server-ver16#returns
%
% "When allocating a handle other than an environment handle, if SQLAllocHandle returns SQL_ERROR, it sets
% OutputHandlePtr to SQL_NULL_HDBC, SQL_NULL_HSTMT, or SQL_NULL_HDESC, depending on the value of HandleType,
% unless the output argument is a null pointer."
%
% This is fine. The ODBC statement pointer is checked for being one of the above null values. When it is null,
% then SQLFreeHandle isn't called.


%----------------------------------------------------------------------------------------------------
% 0. C declarations
%----------------------------------------------------------------------------------------------------

:- pragma foreign_decl("C", "

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

    /*
    ** odbc.m allocates memory within may_call_mercury pragma C code,
    ** which is a bit dodgy in non-conservative GC grades.
    ** Allowing non-conservative GC grades would require a bit of fairly
    ** error-prone code to save/restore the heap pointer in the right
    ** places. When accurate garbage collection is implemented and a
    ** nicer way of allocating heap space from within C code is available,
    ** this should be revisited.
    ** Conservative garbage collection also makes restoring the state
    ** after an exception a bit simpler.
    */
#ifndef MR_CONSERVATIVE_GC
#error ""The OBDC interface requires conservative garbage collection. \\
        Use a compilation grade containing .gc.""
#endif /* ! MR_CONSERVATIVE_GC */

/*
** For use with iODBC.
*/
#ifdef MODBC_IODBC

    #include ""isql.h""
    #include ""isqlext.h""
    /* #include ""odbc_funcs.h"" */
    #include ""sqltypes.h""

    /*
    ** iODBC 2.12 doesn't define this, so define it to something
    ** harmless.
    */
    #ifndef SQL_NO_DATA
        #define SQL_NO_DATA SQL_NO_DATA_FOUND
    #endif

#endif /* MODBC_IODBC */

/*
** For use with unixODBC.
*/
#ifdef MODBC_UNIX

    #include ""sql.h""
    #include ""sqlext.h""
    #include ""sqltypes.h""

#endif /* MODBC_UNIX */

/*
** For interfacing directly with ODBC driver bypassing driver managers
** such as iODBC
*/
#ifdef MODBC_ODBC

    #include ""sql.h""
    #include ""sqlext.h""
    #include ""sqltypes.h""

    #ifndef SQL_NO_DATA
        #define SQL_NO_DATA SQL_NO_DATA_FOUND
    #endif

#endif /* MODBC_ODBC */

#ifdef MODBC_MS

    /*
    ** ODBC_VER set to 0x0250 means that this uses only ODBC 2.0
    ** functionality but compiles with the ODBC 3.0 header files.
    */
    #define ODBC_VER 0x0250

    /*
    ** The following is needed to allow the Microsoft headers to
    ** compile with GNU C under gnu-win32.
    */

    #if defined(__GNUC__) && !defined(__stdcall)
      #define __stdcall __attribute__((stdcall))
    #endif

    #if defined(__CYGWIN32__) && !defined(WIN32)
      #define WIN32 1
    #endif

    #include <windows.h>
    #include ""sql.h""
    #include ""sqlext.h""
    #include ""sqltypes.h""

#endif /* MODBC_MS */

").


%----------------------------------------------------------------------------------------------------
% 1. Results
%----------------------------------------------------------------------------------------------------

% Error level: classification of the success of ODBC operations.

:- type error_level
    ---> okay           % All went smoothly, no warnings or errors.
    ;    warnings       % At least one warning occured, but no errors.
    ;    errors.        % At least one error occured, and warnings may also have occured.


:- pragma foreign_enum("C", error_level/0,
[
    okay     - "OKAY",
    warnings - "WARNINGS",
    errors   - "ERRORS"
]).


:- pragma foreign_decl("C",
"
#define OKAY     ((MR_Word) 0)
#define WARNINGS ((MR_Word) 1)
#define ERRORS   ((MR_Word) 2)

#define max(err1, err2) (((err1) > (err2)) ? (err1) : (err2))
").


db_error_message(odbc_error(Messages)) =
    "Database error(s) found:\n" ++
    string.join_list("\n", map(snd, Messages)) ++ "\n".

db_error_message(conv_error(From, To, Value)) = Str :-
    UStr0 = string.string(Value),
    string.between(UStr0, length("univ_cons("), length(UStr0) - 1, UStr),
    Str = "Inconsistency between the program and the database found.\nError converting from " ++
          From ++ " to " ++ To ++ ".\n" ++
          "The value, which couldn't be converted, is: " ++ UStr ++ ".".

db_error_message(match_error(Row)) =
    "Inconsistency between the program and the database found:\nResult row deviates from what is expected:\n" ++
    "The row is: " ++ string(Row) ++ "\n".

db_error_message(rows_error(Which, Rows)) =
    "Inconsistency between the program and the database found. The number of\n" ++
    "expected rows deviates from what has occured:\n" ++
    rows_error_msg(Which) ++ " but " ++ string.string(length(Rows) : int) ++ " have been received.".

:- func rows_error_msg(rows_error) = string.

rows_error_msg(is_not_one)         = "One row was expected".
rows_error_msg(is_not_at_most_one) = "At most one row was expected".
rows_error_msg(is_not_at_least_one)       = "At least one row was expected".


%----------------------------------------------------------------------------------------------------

% Package a value and the warning/error messages in a result. The messages are included in the order, in which they
% occured.

:- pred make_result(
    T::in,
    error_level::in,
    messages::in,
    odbc.result(T)::out
) is det.

make_result(Val, ErrorLevel, Messages, Result) :-
    (
        ErrorLevel = okay,
        Result = odbc.ok(Val, [])
    ;
        ErrorLevel = warnings,
        Result = odbc.ok(Val, reverse(warnings(Messages)))
    ;
        ErrorLevel = errors,
        Result = odbc.error(reverse(Messages))
    ).



% Package warning/error messages, without any value, in a result.

:- pred make_result(
    error_level::in,
    messages::in,
    odbc.result::out
) is det.

make_result(ErrorLevel, Messages, Result) :-
    (
        ErrorLevel = okay,
        Result = odbc.ok([])
    ;
        ErrorLevel = warnings,
        Result = odbc.ok(reverse(warnings(Messages)))
    ;
        ErrorLevel = errors,
        Result = odbc.error(reverse(Messages))
    ).



% Make an odbc.error/1 from an odbc.state. The state must actually contain an error.

:- pred make_error(odbc.state::in, odbc.result(T)::out) is det.

make_error(odbc.state(_, _, _, Messages), Result) :-
    Result = odbc.error(reverse(Messages)).



%----------------------------------------------------------------------------------------------------

get_result(Res, Val) :-
    (
        Res = ok(Val, _)
    ;
        Res = error(Messages),
        throw(odbc_error(Messages))
    ).


throw_odbc_error(Res, !IO) :-
    (
        Res = odbc.ok(_)
    ;
        Res = odbc.error(Messages),
        throw(odbc_error(Messages))
    ).


throw_odbc_error(Res1, Res2) :-
    (
        Res1 = odbc.ok(_),
        Res2 = Res1
    ;
        Res1 = odbc.error(Messages),
        throw(odbc_error(Messages))
    ).


%----------------------------------------------------------------------------------------------------
% 2. Extracting information from a result set
%----------------------------------------------------------------------------------------------------


% Convert a list of rows to a list of records, with the specified semidet conversion function. When the function
% fails for a row, throw a match_error/1 exception, which includes the offending row.

:- pred transform((func(odbc.row) = T),          list(odbc.row),       list(T)).
:- mode transform(in(func(in) = out is semidet), in,                   out)                   is det.
:- mode transform(in(func(in) = out is semidet), in(non_empty_list),   out(non_empty_list))   is det.

transform(_, [], []).

transform(Extr, [Row|Rows], [Data|Rest]) :-
    (
        if   Data0 = Extr(Row)
        then
             Data = Data0,
             transform(Extr, Rows, Rest)
        else
             throw(match_error(Row))
    ).


:- pred transform1(
    (func(odbc.row) = T)::in(func(in) = out is semidet),
    odbc.row::in,
    T::out
) is det.

transform1(Extr, Row, Data) :-
    (
        if   Data0 = Extr(Row)
        then
             Data = Data0
        else
             throw(match_error(Row))
    ).


rows(Result, Extr, Records) :-
    get_result(Result, Rows),
    transform(Extr, Rows, Records).


rows_one(Result, Extr, One) :-
    get_result(Result, Rows),
    (
        Rows = [ Row ],
        transform1(Extr, Row, One)
    ;
        ( Rows = [] ; Rows = [_,_|_] ),
        throw(rows_error(is_not_one, Rows))
    ).


rows_at_most_one(Result, Extr, AtMostOne) :-
    get_result(Result, Rows),
    (
        Rows = [],
        AtMostOne = no
    ;
        Rows = [ Row ],
        transform1(Extr, Row, One),
        AtMostOne = yes(One)
    ;
        Rows = [_,_|_],
        throw(rows_error(is_not_at_most_one, Rows))
    ).


rows_at_least_one(Result, Extr, Multi) :-
    get_result(Result, Rows),
    (
        Rows = [],
        throw(rows_error(is_not_at_least_one, Rows))
    ;
        Rows = [_|_],
        transform(Extr, Rows, Multi)
    ).


%----------------------------------------------------------------------------------------------------
% 3. Conversion of attribute values
%----------------------------------------------------------------------------------------------------

:- instance conv(string, int) where [
    conv(Str) = Int :-
        (
            if   to_int(Str, Int0)
            then Int = Int0
            else throw(conv_error("string", "int", univ(Str)))
        )
].

:- instance conv(int64,  int) where [
    conv(I) = int64.cast_to_int(I)
].

:- instance conv(uint64, int) where [
    conv(I) = Int :-
        (
            if
                I > 0x7FFFFFFFFFFFFFFFu64
            then
                throw(conv_error("uint64", "int", univ(I)))
            else
                Int = uint64.cast_to_int(I)
        )
].

:- instance conv(int32,  int) where [
    conv(I) = int32.cast_to_int(I)
].

:- instance conv(uint32, int) where [
    conv(I) = uint32.cast_to_int(I)
].

:- instance conv(int16,  int) where [
    conv(I) = int16.cast_to_int(I)
].

:- instance conv(uint16, int) where [
    conv(I) = uint16.cast_to_int(I)
].

:- instance conv(int8,   int) where [
    conv(I) = int8.cast_to_int(I)
].

:- instance conv(uint8,  int) where [
    conv(I) = uint8.cast_to_int(I)
].

:- instance conv(int8,  bool) where [
    conv(Int) = Bool :-
        (
            if  ( Int = 0i8,
                  Bool0 = no
                ; Int = 1i8,
                  Bool0 = yes
                )
            then
                Bool = Bool0
            else
                throw(conv_error("int8", "bool", univ(Int)))
        )
].

:- instance conv(string, string) where [
    conv(Str) = Str
].

:- instance conv(float, float) where [
    conv(Float) = Float
].

:- instance conv(odbc.attribute, int) where [
    conv(Att) = Int :-
        (
            if
                (
                    Att = int64(Int64),
                    Int0 = conv(Int64)
                ;
                    Att = uint64(Uint64) ,
                    Int0 = conv(Uint64)
                ;
                    Att = int32(Int32),
                    Int0 = conv(Int32)
                ;
                    Att = uint32(Uint32),
                    Int0 = conv(Uint32)
                ;
                    Att = int16(Int16),
                    Int0 = conv(Int16)
                ;
                    Att = uint16(Uint16),
                    Int0 = conv(Uint16)
                ;
                    Att = int8(Int8),
                    Int0 = conv(Int8)
                ;
                    Att = uint8(Uint8),
                    Int0 = conv(Uint8)
                )
            then
                Int = Int0
            else
                throw(conv_error("odbc.attribute", "int", univ(Att)))
        )
].


:- instance conv(attribute, int64) where [
    conv(Att) = Val :-
        (
            if
                Att = int64(Val0)
            then
                Val = Val0
            else
                throw(conv_error("odbc.attribute", "int64", univ(Att)))
        )
].

:- instance conv(attribute, uint64) where [
    conv(Att) = Val :-
        (
            if
                Att = uint64(Val0)
            then
                Val = Val0
            else
                throw(conv_error("odbc.attribute", "uint64", univ(Att)))
        )

].

:- instance conv(attribute, int32) where [
    conv(Att) = Val :-
        (
            if
                Att = int32(Val0)
            then
                Val = Val0
            else
                throw(conv_error("odbc.attribute", "int32", univ(Att)))
        )
].

:- instance conv(attribute, uint32) where [
    conv(Att) = Val :-
        (
            if
                Att = uint32(Val0)
            then
                Val = Val0
            else
                throw(conv_error("odbc.attribute", "uint32", univ(Att)))
        )
].

:- instance conv(attribute, int16) where [
    conv(Att) = Val :-
        (
            if
                Att = int16(Val0)
            then
                Val = Val0
            else
                throw(conv_error("odbc.attribute", "int16", univ(Att)))
        )
].

:- instance conv(attribute, uint16) where [
    conv(Att) = Val :-
        (
            if
                Att = uint16(Val0)
            then
                Val = Val0
            else
                throw(conv_error("odbc.attribute", "uint16", univ(Att)))
        )
].

:- instance conv(attribute, int8) where [
    conv(Att) = Val :-
        (
            if
                Att = int8(Val0)
            then
                Val = Val0
            else
                throw(conv_error("odbc.attribute", "int8", univ(Att)))
        )
].

:- instance conv(attribute, uint8) where [
    conv(Att) = Val :-
        (
            if
                Att = uint8(Val0)
            then
                Val = Val0
            else
                throw(conv_error("odbc.attribute", "uint8", univ(Att)))
        )
].

:- instance conv(odbc.attribute, string) where [
    conv(Att) = Str :-
        (
            if
                (
                    Att = string(Str1),
                    Str0 = Str1
                ;
                    Att = time(Timestamp),
                    Str0 = Timestamp
                )
            then
                Str = Str0
            else
                throw(conv_error("odbc.attribute", "string", univ(Att)))
        )
].


:- instance conv(odbc.attribute, float) where [
    conv(Att) = Float :-
        (
            if
                (
                    Att = float(Float1),
                    Float0 = Float1
                )
            then
                Float = Float0
            else
                throw(conv_error("odbc.attribute", "float", univ(Att)))
        )
].


% For each conversion function for attributes that can't be NULL, make a conversion function for an attribute that
% can be NULL.
:- instance conv(attribute, maybe(T)) <= conv(attribute, T) where [
    conv(Att) = MaybeT :-
        (
            if   Att = null
            then MaybeT = no
            else MaybeT = yes(conv(Att))
        )
].


%----------------------------------------------------------------------------------------------------
% 4. Environments and Connections
%----------------------------------------------------------------------------------------------------

:- type env.

:- pragma foreign_type("C", env, "SQLHENV").

:- type environment
    ---> environment(
        env
    ).


:- type conn.

:- pragma foreign_type("C", conn, "SQLHDBC").

:- type connection
    ---> connection(
        env,
        conn
    ).


%----------------------------------------------------------------------------------------------------

% Create an ODBC environment.

open_environment(Result, !IO) :-
    c_open_environment(Env, ErrorLevel, Messages, !IO),
    make_result(environment(Env), ErrorLevel, Messages, Result).


:- pred c_open_environment(env::out, error_level::out, list(odbc.message)::out, io::di, io::uo) is det.

:- pragma foreign_proc("C",
    c_open_environment(Env::out, ErrorLevel::out, Messages::out, IO0::di, IO1::uo),
    [ thread_safe, promise_pure, may_call_mercury, will_not_throw_exception ],
"
    SQLHENV   env = SQL_NULL_HANDLE;
    SQLRETURN ret;
    int       okay;

    ErrorLevel = OKAY;
    Messages = MR_list_empty();

    ret = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &env);
    okay = odbc_check(env, SQL_NULL_HANDLE, SQL_NULL_HANDLE, ret, &ErrorLevel, &Messages);

    if (okay) {
        ret = SQLSetEnvAttr(env, SQL_ATTR_ODBC_VERSION, (void*)SQL_OV_ODBC3, 0);

        if (!odbc_check(env, SQL_NULL_HANDLE, SQL_NULL_HANDLE, ret, &ErrorLevel, &Messages)) {
            // The SQLAllocHandle() call succeeded, but the SQLSetEnvAttr() failed. Close the handle so we don't
            // get a resource leak.
            SQLFreeHandle(SQL_HANDLE_ENV, env);
        }
    }

    Env = env;
    IO1 = IO0;
").


%----------------------------------------------------------------------------------------------------

% Close an ODBC environment. This just frees the handle.

close_environment(environment(Env), Result, !IO) :-
    c_close_environment(Env, ErrorLevel, Messages, !IO),
    make_result(ErrorLevel, Messages, Result).


:- pred c_close_environment(
    env::in,            %
    error_level::out,
    list(odbc.message)::out,
    io::di, io::uo
) is det.

:- pragma foreign_proc("C",
    c_close_environment(Env::in, ErrorLevel::out, Messages::out, IO0::di, IO1::uo),
    [ thread_safe, promise_pure, may_call_mercury, will_not_throw_exception ],
"
    SQLRETURN ret;

    Messages = MR_list_empty();
    ErrorLevel = OKAY;

    if (Env != NULL) {
        ret = SQLFreeHandle(SQL_HANDLE_ENV, Env);

        switch(ret)
        {
        case SQL_SUCCESS:
            break;

        case SQL_ERROR:
        case SQL_INVALID_HANDLE:
            odbc_check(Env, SQL_NULL_HANDLE, SQL_NULL_HANDLE, ret, &ErrorLevel, &Messages);
            break;

        /* SQLFreeHandle doesn't return SQL_SUCCESS_WITH_INFO. */
        }
    }

    IO1 = IO0;
").


%----------------------------------------------------------------------------------------------------


% Open an ODBC connection in the specified environment.

open_connection(environment(Env), DataSource, User, Password, Result, !IO) :-
    Connection = connection(Env, Conn),
    c_open_connection(Env, DataSource, User, Password, Conn, ErrorLevel, Messages, !IO),
    make_result(Connection, ErrorLevel, Messages, Result).

:- pred c_open_connection(
    env::in,                              % ODBC environment
    string::in, string::in, string::in,   % Connect data
    conn::out,                            % ODBC Connection
    error_level::out,                     % What kind of errors occured
    list(odbc.message)::out,              % Error messages (if any), in reverse order
    io::di, io::uo
) is det.

:- pragma foreign_proc("C",
    c_open_connection(Env::in, DataSource::in, User::in, Password::in,
              Conn::out, ErrorLevel::out, Messages::out, IOin::di, IOout::uo),
    [ thread_safe, may_call_mercury, will_not_throw_exception, promise_pure, may_not_duplicate ],
"
    SQLHDBC   conn = SQL_NULL_HANDLE;
    SQLRETURN rc;
    int       okay = 1;

    Messages = MR_list_empty();
    ErrorLevel = OKAY;

    rc = SQLAllocHandle(SQL_HANDLE_DBC, Env, &conn);
    okay = odbc_check(Env, SQL_NULL_HANDLE, SQL_NULL_HANDLE, rc, &ErrorLevel, &Messages);
    if (!okay) goto end;

    rc = SQLConnect(
        conn,
        (SQLCHAR*) DataSource, SQL_NTS,
        (SQLCHAR*) User, SQL_NTS,
        (SQLCHAR*) Password, SQL_NTS);

    okay = odbc_check(Env, conn, SQL_NULL_HANDLE, rc, &ErrorLevel, &Messages);
    if (!okay) {
        SQLFreeHandle(SQL_HANDLE_DBC, conn);
        goto end;
    }

    // Turn off auto-commit mode. We do explicit commits and rollbacks.
    rc = SQLSetConnectAttr(conn, SQL_ATTR_AUTOCOMMIT, SQL_AUTOCOMMIT_OFF, 0);
    okay = odbc_check(Env, conn, SQL_NULL_HANDLE, rc, &ErrorLevel, &Messages);

    if (!okay) {
        SQLDisconnect(conn);
        SQLFreeHandle(SQL_HANDLE_DBC, conn);
        goto end;
    }

    end:
    Conn = conn;
    IOout = IOin;
").


%----------------------------------------------------------------------------------------------------

% Close an ODBC connection.

close_connection(Connection, Result, !IO) :-
    Connection = connection(Env, Conn),
    c_close_connection(Env, Conn, ErrorLevel, Messages, !IO),
    make_result(ErrorLevel, Messages, Result).

:- pred c_close_connection(
    env::in,                    % ODBC environment
    conn::in,                   % ODBC connection
    error_level::out,           % If an error occured
    list(odbc.message)::out,    % Error messages (if any)
    io::di, io::uo
) is det.

:- pragma foreign_proc("C",
    c_close_connection(Env::in, Conn::in, ErrorLevel::out, Messages::out, IOin::di, IOout::uo),
    [ thread_safe, promise_pure, may_call_mercury, will_not_throw_exception ],
"
    SQLRETURN ret = SQL_SUCCESS;
    int okay;

    Messages = MR_list_empty();
    ErrorLevel = OKAY;

    ret = SQLDisconnect(Conn);
    okay = odbc_check(Env, Conn, SQL_NULL_HANDLE, ret, &ErrorLevel, &Messages);

    if (okay && Conn != SQL_NULL_HDBC) {
        ret = SQLFreeHandle(SQL_HANDLE_DBC, Conn);
        odbc_check(Env, Conn, SQL_NULL_HANDLE, ret, &ErrorLevel, &Messages);
    }

    IOout = IOin;
").



%----------------------------------------------------------------------------------------------------

with_environment(Pred, Out, !IO) :-
    open_environment(Result, !IO),
    get_result(Result, Env),

    finally(
        (pred(Out1::out, !.IO::di, !:IO::uo) is cc_multi :-
            Pred(Env, Out1, !IO)
        ),
        Out,
        (pred(io.ok::out, !.IO::di, !:IO::uo) is cc_multi :-
            close_environment(Env, Result1, !IO),
            throw_odbc_error(Result1, !IO)
        ),
        _,
        !IO
    ).


with_connection(Environment, DataSource, User, Password, Pred, Out, !IO) :-
    open_connection(Environment, DataSource, User, Password, Result, !IO),
    get_result(Result, Conn),

    finally(
        (pred(Out1::out, !.IO::di, !:IO::uo) is cc_multi :-
            Pred(Conn, Out1, !IO)
        ),
        Out,
        (pred(io.ok::out, !.IO::di, !:IO::uo) is cc_multi :-
            close_connection(Conn, Result1, !IO),
            throw_odbc_error(Result1, !IO)
        ),
        _,
        !IO
    ).


with_connection(DataSource, User, Password, Pred, Out, !IO) :-
    with_environment(
        (pred(Environment::in, Out1::out, !.IO::di, !:IO::uo) is cc_multi :-
            with_connection(Environment, DataSource, User, Password, Pred, Out1, !IO)),
        Out,
        !IO).





%----------------------------------------------------------------------------------------------------


:- pragma foreign_code("C", "

// Does the data have no maximum length?
// Note: the implementation of SQLGetData for MySQL does not follow the same
// standard as SQL Server, but from examination of the sources SQLDescribeCol
// seems guaranteed to find the maximum length of a result column containing
// variable length data. SQL_NO_TOTAL, which should be returned if the length
// cannot be determined, is not defined by the iODBC header files.
MR_bool
odbc_is_variable_length_sql_type(SWORD sql_type) {
#ifdef MODBC_MYSQL

    return MR_FALSE;

#else // ! MODBC_MYSQL

    return (
        sql_type == SQL_LONGVARBINARY ||
        sql_type == SQL_LONGVARCHAR
    );

#endif // !MODBC_MYSQL
}
").

%----------------------------------------------------------------------------------------------------

:- pragma foreign_code("C", "
// Map an ODBC SQL type to a supported attribute type.
// Currently, supported attribute types are minimal,
// but this function will allow us to ask ODBC to make
// conversion from SQL types to supported types.
// Binary types are currently converted to strings.

MODBC_AttrType
odbc_sql_type_to_attribute_type(SWORD sql_type, int is_unsigned)
{
    switch (sql_type) {
        case SQL_BIGINT:        return (is_unsigned ? MODBC_UINT64 : MODBC_INT64);
        case SQL_BINARY:        return MODBC_STRING;
        case SQL_BIT:           return MODBC_BIT;
        case SQL_CHAR:          return MODBC_STRING;
        case SQL_DECIMAL:       return MODBC_STRING;
        case SQL_DOUBLE:        return MODBC_FLOAT;
        case SQL_FLOAT:         return MODBC_FLOAT;
        case SQL_INTEGER:       return (is_unsigned ? MODBC_UINT32 : MODBC_INT32);

    // For MySQL, SQLGetData does not work correctly (multiple calls
    // return the same data, not successive pieces of the data).
    // It seems to be guaranteed to be able to find the maximum length
    // of the data in the column, so we treat those columns as if
    // they were fixed length.
#ifdef MODBC_MYSQL
        case SQL_LONGVARBINARY:     return MODBC_STRING;
        case SQL_LONGVARCHAR:       return MODBC_STRING;
#else // ! MODBC_MYSQL
        case SQL_LONGVARBINARY:     return MODBC_VAR_STRING;
        case SQL_LONGVARCHAR:       return MODBC_VAR_STRING;
#endif // ! MODBC_MYSQL

        case SQL_NUMERIC:       return MODBC_STRING;
        case SQL_REAL:          return MODBC_FLOAT;
        case SQL_SMALLINT:      return (is_unsigned ? MODBC_UINT16 : MODBC_INT16);
        case SQL_TYPE_DATE:     return MODBC_TIME;
        case SQL_TYPE_TIME:          return MODBC_TIME;
        case SQL_TYPE_TIMESTAMP:     return MODBC_TIME;
        case SQL_TIMESTAMP:     return MODBC_TIME;
        case SQL_TINYINT:       return (is_unsigned ? MODBC_UINT8 : MODBC_INT8);
        case SQL_VARBINARY:     return MODBC_STRING;
        case SQL_VARCHAR:       return MODBC_STRING;

        case SQL_DATETIME:      return MODBC_TIME;

        default:
            //printf(""odbc_sql_type_to_attribute_type: sql_type=%d\\n"", sql_type); /*XXX*/
            MR_external_fatal_error(""odbc.m"",
                ""sql_type_to_attribute_type: unknown type"");
    }
}

// Return the SQL_C type corresponding to a supported attribute type.
SWORD
odbc_attribute_type_to_sql_c_type(MODBC_AttrType AttrType)
{
    switch (AttrType) {
        case MODBC_FLOAT:       return SQL_C_DOUBLE;
        case MODBC_TIME:        return SQL_C_CHAR;
        case MODBC_STRING:      return SQL_C_CHAR;
        case MODBC_VAR_STRING:  return SQL_C_CHAR;
        case MODBC_INT64:       return SQL_C_SBIGINT;
        case MODBC_UINT64:      return SQL_C_UBIGINT;
        case MODBC_INT32:       return SQL_C_SLONG;
        case MODBC_UINT32:      return SQL_C_ULONG;
        case MODBC_INT16:       return SQL_C_SSHORT;
        case MODBC_UINT16:      return SQL_C_USHORT;
        case MODBC_INT8:        return SQL_C_STINYINT;
        case MODBC_UINT8:       return SQL_C_UTINYINT;
        default:
            // Unsupported MODBC_xxx type.
            MR_external_fatal_error(""odbc.m"",
                ""attribute_type_to_sql_c_type: unknown type"");
    }
}


// This function computes to total number of bytes needed
// to store an attribute value, returning -1 if there is no
// maximum size.
// [SqlType] is the ODBC SQL type of the column
// [cbColDef] is the size returned by SQLDescribeCol
// [ibScaler] is the scale returned by SQLDescribeCol
// [fNullable] is whether the column can be NULL
size_t
odbc_sql_type_to_size(SWORD sql_type, UDWORD cbColDef,
        SWORD ibScale, SWORD fNullable)
{
    switch (sql_type)
    {
        // 64-bit signed int converted to an int64
        case SQL_BIGINT:
            return sizeof(MODBC_C_INT64);

        // Binary data converted to SQL_C_CHAR
        // Each byte is converted to 2-digit Hex.
        case SQL_BINARY:
            return cbColDef * 2 + 1;  // +1 for NUL

        // Bit converted to SQL_C_TINYINT
        case SQL_BIT:
            return 1;   //XXX

        // Fixed char to SQL_C_CHAR.
        case SQL_CHAR:
            return cbColDef + 1;  // 1 for NUL

        // Date YYYY-MM-DD converted to SQL_C_CHAR.
        case SQL_DATE:
            return cbColDef + 1;  // 1 for NUL

        // Signed decimal ddd.dd converted to SQL_C_CHAR.
        case SQL_DECIMAL:
            return 1 + cbColDef + 1 + ibScale + 1;
            // 1 for sign 1, 1 for decimal point, 1, for NUL

        // 32-bit float converted to MODBC_SQL_C_FLOAT.
        case SQL_DOUBLE:
            return sizeof(MODBC_C_FLOAT);

        // 32-bit float converted to MODBC_SQL_C_FLOAT.
        case SQL_FLOAT:
            return sizeof(MODBC_C_FLOAT);

        // 32-bit integer converted to SQL_C_SINT
        case SQL_INTEGER:
            return sizeof(MODBC_C_INT32);

        // Any length binary convert to SQL_C_CHAR
        // For MySQL, there are no column types for
        // which the maximum length cannot be determined before
        // starting to fetch data, hence the #ifdefs below.
        case SQL_LONGVARBINARY:
#ifdef MODBC_MYSQL
            return cbColDef * 2 + 1;    // 1 for NUL
#else // !MODBC_MYSQL
            return -1;
#endif // !MODBC_MYSQL

        // Any length char convert to SQL_C_CHAR
        // For MySQL, there are no column types for
        // which the maximum length cannot be determined before
        // starting to fetch data, hence the #ifdefs below.
        case SQL_LONGVARCHAR:
#ifdef MODBC_MYSQL
            return cbColDef + 1;    // 1 for NUL
#else // !MODBC_MYSQL
            return -1;
#endif // !MODBC_MYSQL

        // Signed numeric ddd.dd converted to SQL_C_CHAR.
        case SQL_NUMERIC:
            return 1 + cbColDef + 1 + ibScale + 1; // 1 for NUL

        // 32-bit float converted to MODBC_SQL_C_FLOAT.
        case SQL_REAL:
            return sizeof(MODBC_C_FLOAT);

        // 16-bit integer converted to SQL_C_SHORT
        case SQL_SMALLINT:
            return sizeof(MODBC_C_INT16);

        // Time hh:mm:ss converted to SQL_C_CHAR.
        case SQL_TIME:
            return cbColDef + 1;  // 1 for NUL

        // Time YYYY-MM-DD hh:mm:ss converted to SQL_C_CHAR.
        case SQL_TIMESTAMP:
            return cbColDef + 1;  // 1 for NUL

        case SQL_TYPE_TIMESTAMP:
            return cbColDef + 1;  // 1 for NUL

        // 8-bit integer converted to MODBC_C_INT8.
        case SQL_TINYINT:
            return sizeof(MODBC_C_INT8);

        // Binary data converted to SQL_C_CHAR.
        // Each byte is converted to 2-digit Hex.
        case SQL_VARBINARY:
            return cbColDef * 2 + 1;  // 1 for NUL

        // Fixed char to SQL_C_CHAR.
        case SQL_VARCHAR:
            return cbColDef + 1;  // 1 for NUL

        default:
            //printf(""odbc_sql_type_to_size: sql_type=%d\\n"", sql_type); /*XXX*/
            MR_external_fatal_error(""odbc.m"",
                ""sql_type_to_size: unknown type"");
    }
}
").


%----------------------------------------------------------------------------------------------------
% 5. Transactions
%----------------------------------------------------------------------------------------------------


% The database state. This is used only inside of transactions, in the predicates provided to the transaction/5
% predicate.

:- type odbc.state
    ---> state(
        odbc.env,       % ODBC environment
        odbc.conn,      % ODBC connection
        error_level,    % Error level
        messages        % ODBC error and warning messages in reverse order
    ).


% Exception thrown to roll back and abort a transaction, with the rollback/3 call. Carries the error message to
% include in the result of transaction/5.

:- type rollback_exc
    ---> rollback_exc(string).


transaction(Connection, Trans, Result, !IO) :-
    some [!DB] (
        Connection = connection(Env, Conn),
        !:DB = odbc.state(Env, Conn, okay, []),
        (
            try [io(!IO)]
                (
                    % Execute the transaction and commit.
                    unsafe_promise_unique(!.DB, !:DB),
                    Trans(Result0, !DB),
                    commit(!DB),
                    !.DB = odbc.state(_, _, ErrorLevel, Messages),
                    make_result(Result0, ErrorLevel, Messages, Result)
                )

            then
                true

            catch rollback_exc(Msg) ->
                % Caught a rollback_exc exception, which is thrown by the rollback/3 predicate, to roll back and
                % abort a transaction.
                unsafe_promise_unique(!.DB, !:DB),
                update_error_state(errors, [error(user_requested_rollback) - Msg], !DB),
                rollback(!DB, !IO),
                !.DB = odbc.state(_, _, _, Messages),
                Result = (odbc.error(reverse(Messages)) : odbc.result(T))

            catch_any Exception ->
                % Caught an arbitrary exception thrown by the transaction predicate (Trans). This causes the
                % transaction to be rolled back. Then the exception is rethrown. The messages from the rollback
                % operation are lost.
                unsafe_promise_unique(!.DB, !:DB),
                rollback(!.DB, _, !IO),
                throw(Exception)
        )
    ).



%----------------------------------------------------------------------------------------------------

:- pred commit(
    odbc.state::di, odbc.state::uo
) is det.

commit(!DB) :-
    !.DB = odbc.state(_, Conn, _, _),
    unsafe_promise_unique(!.DB, !:DB),
    c_commit(Conn, !DB, ErrorLevel, Messages),
    update_error_state(ErrorLevel, Messages, !DB).


:- pred c_commit(conn::in, odbc.state::di, odbc.state::uo, ErrorLevel::out, Messages::out) is det.

:- pragma foreign_proc("C",
    c_commit(Conn::in, DB0::di, DB::uo, ErrorLevel::out, Messages::out),
    [ thread_safe, promise_pure, may_call_mercury, will_not_throw_exception ],
"
    SQLRETURN rc;

    ErrorLevel = OKAY;
    Messages = MR_list_empty();

    rc = SQLEndTran(SQL_HANDLE_DBC, Conn, SQL_COMMIT);
    odbc_check(Conn, SQL_NULL_HANDLE, SQL_NULL_HANDLE, rc, &ErrorLevel, &Messages);

    DB = DB0;
").


%----------------------------------------------------------------------------------------------------

rollback(Msg, !DB) :-
    throw(rollback_exc(Msg)).


% This rollback is called by transaction/5, after catching a rollback_exc exception. It adds the warnings/errors
% which occured in the SQLEndTran() function.
%
% This predicate depends on the IO state, even though it doesn't need it. The reason is, that the compiler must be
% kept from optimising the call away, when there isn't done anything with the DB state, after it has been returned
% form fallback/4. This would happen in transaction/5 in the "catch_any" branch.

:- pred rollback(
    odbc.state::di, odbc.state::uo, io::di, io::uo
) is det.

rollback(!DB, !IO) :-
    clear(!DB, _, Conn),
    unsafe_promise_unique(!.DB, !:DB),
    c_rollback(Conn, !DB, !IO, ErrorLevel, Messages),
    update_error_state(ErrorLevel, Messages, !DB).


:- pred c_rollback(
    conn::in,                           % Database connection
    odbc.state::di, odbc.state::uo,     % Database state
    io::di, io::uo,                     % World state
    ErrorLevel::out, Messages::out      % Error handling
) is det.

:- pragma foreign_proc("C",
    c_rollback(Conn::in, DB0::di, DB::uo, IO0::di, IO::uo, ErrorLevel::out, Messages::out),
    [ thread_safe, promise_pure, may_call_mercury, will_not_throw_exception ],
"
    SQLRETURN rc;

    ErrorLevel = OKAY;
    Messages = MR_list_empty();

    rc = SQLEndTran(SQL_HANDLE_DBC, Conn, SQL_ROLLBACK);
    odbc_check(Conn, SQL_NULL_HANDLE, SQL_NULL_HANDLE, rc, &ErrorLevel, &Messages);

    DB = DB0;
    IO = IO0;
").


%----------------------------------------------------------------------------------------------------

% Query the supported transaction isolation levels

get_transaction_isolation_levels(Connection, Result, !IO) :-
    Connection = connection(Env, Conn),
    c_get_transaction_isolation_levels(
        Env, Conn,
        Read_uncommitted, Read_committed, Repeatable_read, Serializable,
        ErrorLevel, Messages, !IO),
    Val = transaction_isolation_levels(Read_uncommitted, Read_committed, Repeatable_read, Serializable),
    make_result(Val, ErrorLevel, Messages, Result).


:- pred c_get_transaction_isolation_levels(
    env::in,                    % ODBC environment
    conn::in,                   % ODBC connection
    bool::out,                  % Read_uncommitted
    bool::out,                  % Read_committed
    bool::out,                  % Repeatable_read
    bool::out,                  % Serializable
    error_level::out,           % If an error occured
    list(odbc.message)::out,    % Error messages (if any)
    io::di, io::uo
) is det.

:- pragma foreign_proc("C",
    c_get_transaction_isolation_levels(
        Env::in, Conn::in,
        ReadUncommitted::out, ReadCommitted::out, RepeatableRead::out, Serializable::out,
        ErrorLevel::out, Messages::out, IOin::di, IOout::uo),
    [ thread_safe, may_call_mercury, promise_pure, will_not_throw_exception ],
"
    SQLRETURN       ret = SQL_SUCCESS;
    SQLUINTEGER     InfoValue;
    SQLSMALLINT     StringLength;

    Messages = MR_list_empty();
    ErrorLevel = OKAY;

    ReadUncommitted = MR_FALSE;
    ReadCommitted   = MR_FALSE;
    RepeatableRead  = MR_FALSE;
    Serializable    = MR_FALSE;

    int okay;

    ret = SQLGetInfo(Conn, SQL_TXN_ISOLATION_OPTION, &InfoValue, sizeof(InfoValue), &StringLength);
    okay = odbc_check(Env, Conn, SQL_NULL_HANDLE, ret, &ErrorLevel, &Messages);

    if (okay) {
        if (InfoValue & SQL_TXN_READ_UNCOMMITTED) {
            ReadUncommitted = MR_TRUE;
        }
        if (InfoValue & SQL_TXN_READ_COMMITTED) {
            ReadCommitted = MR_TRUE;
        }
        if (InfoValue & SQL_TXN_REPEATABLE_READ) {
            RepeatableRead = MR_TRUE;
        }
        if (InfoValue & SQL_TXN_SERIALIZABLE) {
            Serializable = MR_TRUE;
        }
    }

    IOout = IOin;
").


%----------------------------------------------------------------------------------------------------

% Set the transaction isolation level for the connection.

set_transaction_isolation_level(Connection, TIL, Result, !IO) :-
    Connection = connection(Env, Conn),

    % Can't use a foreign enum here, because the ODBC values are pointers.
    ( TIL = read_uncommitted, Nr = 1
    ; TIL = read_committed,   Nr = 2
    ; TIL = repeatable_read,  Nr = 3
    ; TIL = serializable,     Nr = 4
    ),

    c_set_transaction_isolation_level(
        Env, Conn,
        Nr,
        ErrorLevel, Messages, !IO),
    make_result(ErrorLevel, Messages, Result).


:- pred c_set_transaction_isolation_level(
    env::in,                            % ODBC environment
    conn::in,                           % ODBC connection
    int::in,                            % Number of new transaction_isolation_level
    error_level::out,                   % If an error occured
    list(odbc.message)::out,            % Error messages (if any)
    io::di, io::uo
) is det.

:- pragma foreign_proc("C",
    c_set_transaction_isolation_level(
        Env::in, Conn::in,
        Nr::in,
        ErrorLevel::out, Messages::out, IOin::di, IOout::uo),
    [ thread_safe, may_call_mercury, promise_pure, will_not_throw_exception ],
"
    SQLRETURN       ret = SQL_SUCCESS;
    SQLPOINTER      Value = 0;

    switch(Nr) {
        case 1: Value = (SQLPOINTER) SQL_TXN_READ_UNCOMMITTED; break;
        case 2: Value = (SQLPOINTER) SQL_TXN_READ_COMMITTED; break;
        case 3: Value = (SQLPOINTER) SQL_TXN_REPEATABLE_READ; break;
        case 4: Value = (SQLPOINTER) SQL_TXN_SERIALIZABLE; break;
    }

    Messages = MR_list_empty();
    ErrorLevel = OKAY;

    ret = SQLSetConnectAttr(Conn, SQL_ATTR_TXN_ISOLATION, Value, 0);
    odbc_check(Env, Conn, SQL_NULL_HANDLE, ret, &ErrorLevel, &Messages);

    IOout = IOin;
").



%----------------------------------------------------------------------------------------------------
% 6. Queries
%----------------------------------------------------------------------------------------------------

execute(SQL, Result, !DB) :-
    some [!Statement] (
        !.DB = odbc.state(Env, Conn, _, _),
        alloc_statement(Env, Conn, !:Statement, !:DB),
        unsafe_promise_unique(!.DB, !:DB),

        chain(prepare_statement(SQL),     !Statement, !DB),
        chain(execute_statement,          !Statement, !DB),
        chain(get_row_count, RowCount, 0, !Statement, !DB),
        cleanup_statement(!.Statement, !DB),

        !.DB = odbc.state(_, _, ErrorLevel, Messages),
        unsafe_promise_unique(!.DB, !:DB),
        make_result(RowCount, ErrorLevel, Messages, Result)
    ).


solutions(SQL, Result, !DB) :-
    some [!Statement] (
        !.DB = odbc.state(Env, Conn, _, _),
        alloc_statement(Env, Conn, !:Statement, !:DB),

        chain(prepare_statement(SQL), !Statement, !DB),
        chain(execute_statement,      !Statement, !DB),
        chain(bind_columns,           !Statement, !DB),
        chain(get_rows, RowsRev, [],  !Statement, !DB),
        cleanup_statement(!.Statement, !DB),

        !.DB = odbc.state(_, _, ErrorLevel, Messages),
        unsafe_promise_unique(!.DB, !:DB),
        make_result(reverse(RowsRev), ErrorLevel, Messages, Result)
    ).


seq_begin(SQL, Statement, Result, !DB) :-
    some [!Stat] (
        !.DB = odbc.state(Env, Conn, _, _),
        alloc_statement(Env, Conn, !:Stat, !:DB),  % This clears !DB

        chain(prepare_statement(SQL), !Stat, !DB),
        chain(execute_statement,      !Stat, !DB),
        chain(bind_columns,           !Stat, !DB),

        !.DB = odbc.state(_, _, ErrorLevel, Messages),
        unsafe_promise_unique(!.DB, !:DB),

        Statement = !.Stat,
        make_result(ErrorLevel, Messages, Result)
    ).


seq_get(Result, !Statement, !DB) :-
    clear(!DB, _, _),
    chain(get_number_of_columns, NumColumns, 0, !Statement, !DB),
    unsafe_promise_unique(!.DB, !:DB),

    % Try to fetch a new row. This frees and makes the statement invalid when there are no more rows.
    chain(fetch_row, Status, 0, !Statement, !DB),

    !.DB = odbc.state(_, _, ErrorLevel, Messages),
    (
        % A new row has been fetched, or there are no new rows.
        ( ErrorLevel = okay ; ErrorLevel = warnings ),
        (
            if   no_data(Status)
            then Result = ok(no, warnings(reverse(Messages)))
            else unsafe_promise_unique(!.DB, !:DB),
                 chain(get_attributes(1, NumColumns), Row, [], !Statement, !DB),
                 Result = ok(yes(Row), warnings(reverse(Messages)))
        )
    ;
        % An error has occured. The statement data has been cleaned up and the statement has become invalid.
        ErrorLevel = errors,
        Result = error(Messages)
    ),
    unsafe_promise_unique(!.DB, !:DB).


seq_end(Result, Statement, !DB) :-
    clear(!DB, _, _),
    (
        if
            is_null(Statement)
        then
            Result = odbc.ok([])
        else
            cleanup_statement(Statement, !DB),
            !.DB = odbc.state(_, _, ErrorLevel, Messages),
            unsafe_promise_unique(!.DB, !:DB),
            make_result(ErrorLevel, Messages, Result)
    ).


with_statement(SQL, Pred, Result1, MaybeResult23, !DB) :-
    some [!Statement] (
        seq_begin(SQL, !:Statement, Result1, !DB),
        (
            Result1 = odbc.error(_),
            MaybeResult23 = no
        ;
            Result1 = odbc.ok(_),
            (
                try [] (
                    unsafe_promise_unique(!Statement),
                    unsafe_promise_unique(!DB),
                    Pred(Res2, !Statement, !DB)
                )
                then
                    unsafe_promise_unique(!Statement),
                    unsafe_promise_unique(!DB),
                    seq_end(Res3, !.Statement, !DB),
                    MaybeResult23 = yes({ Res2, Res3 })
                catch_any Exc ->
                    unsafe_promise_unique(!Statement),
                    unsafe_promise_unique(!DB),
                    seq_end(Res3, !.Statement, !DB),
                    throw({ Result1, Exc, Res3 })
            )
        )
    ).


% Check for invalid (NULL pointer) statement.

:- pred odbc.is_null(odbc.statement::in) is semidet.

:- pragma foreign_proc("C",
    odbc.is_null(Statement::in),
    [ thread_safe, promise_pure, will_not_call_mercury, will_not_throw_exception ],
"
    SUCCESS_INDICATOR = (Statement == NULL);
").



%----------------------------------------------------------------------------------------------------
% Statements

% Make a new statement data structure. This includes an ODBC statement handle. When this fails (because of
% insufficient memory, for instance) then the returned handle will be SQL_NULL_HSTMT. cleanup_statement checks the
% handle for not being SQL_NULL_HSTMT, before calling SQLFreeHandle() on it.

:- pred alloc_statement(
    odbc.env::in,               % ODBC environment
    odbc.conn::in,              % ODBC connection
    odbc.statement::uo,         % New unique ODBC environment
    odbc.state::uo              % New unique database state
) is det.


% Make a new MODBC_Statement data structure.
alloc_statement(Env, Conn, Statement, DBout) :-
    unsafe_promise_unique(odbc.state(Env, Conn, ErrorLevel, Messages), DBout),
    c_alloc_statement(Env, Conn, Statement, ErrorLevel, Messages).


:- pred c_alloc_statement(
    env::in,                            % environment handle
    conn::in,                           % connection handle
    statement::uo,                      % new MODBC_Statement data structure
    error_level::out, messages::out     % Error handling
) is det.

:- pragma foreign_proc("C",
    c_alloc_statement(Env::in, Conn::in, Statement::uo, ErrorLevel::out, Messages::out),
    [ thread_safe, promise_pure, may_call_mercury, will_not_throw_exception ],
"
    SQLRETURN rc;

    Messages = MR_list_empty();
    ErrorLevel = OKAY;

    // Doing manual deallocation of the statement object.
    Statement = MR_GC_NEW(MODBC_Statement);

    Statement->num_columns = 0;
    Statement->row = NULL;
    Statement->num_rows = 0;
    Statement->stat_handle = SQL_NULL_HSTMT;

    rc = SQLAllocHandle(SQL_HANDLE_STMT, Conn, &(Statement->stat_handle));
    odbc_check(Env, Conn, Statement->stat_handle, rc, &ErrorLevel, &Messages);
").


%----------------------------------------------------------------------------------------------------

% This does the ODBC preparation of a statement (an SQL query). This influences the statement handle in the
% MODBC_Statement data structure. In case of an error, !:Statement becomes a null pointer and !DB is updated
% accordingly.

:- pred prepare_statement(
    string::in,                         % SQL code
    statement::di, statement::uo,       %
    odbc.state::di, odbc.state::uo      %
) is det.

prepare_statement(SQL, !Statement, !DB) :-
    c_prepare_statement(SQL, !Statement, ErrorLevel, Messages),
    unsafe_promise_unique(!.Statement, !:Statement),
    update_error_state(ErrorLevel, Messages, !DB).


:- pred c_prepare_statement(
    string::in,
    statement::di, statement::uo,
    error_level::out, messages::out
) is det.

:- pragma foreign_proc("C",
    c_prepare_statement(
        SQL::in,
        Statement0::di, Statement::uo,
        ErrorLevel::out, Messages::out),
    [ thread_safe, promise_pure, may_call_mercury, will_not_throw_exception ],
"
    SQLRETURN rc;
    MODBC_Statement *statement;

    Messages = MR_list_empty();
    ErrorLevel = OKAY;

    statement = (MODBC_Statement *) Statement0;

    rc = SQLPrepare(statement->stat_handle, (SQLCHAR*) SQL, strlen(SQL));

    if (! odbc_check(SQL_NULL_HANDLE, SQL_NULL_HANDLE, statement->stat_handle, rc, &ErrorLevel, &Messages)) {
        Statement = NULL;
    } else {
        Statement = Statement0;
    }
").



%----------------------------------------------------------------------------------------------------

:- pred execute_statement(
    statement::di, statement::uo,
    odbc.state::di, odbc.state::uo
) is det.

execute_statement(!Statement, !DB) :-
    c_execute_statement(!Statement, ErrorLevel, Messages),
    unsafe_promise_unique(!.Statement, !:Statement),
    update_error_state(ErrorLevel, Messages, !DB).


:- pred c_execute_statement(statement::di, statement::uo, error_level::out, messages::out) is det.

:- pragma foreign_proc("C",
    c_execute_statement(Statement0::di, Statement::uo, ErrorLevel::out, Messages::out),
    [ thread_safe, promise_pure, may_call_mercury, will_not_throw_exception ],
"
    SQLRETURN rc;

    Messages = MR_list_empty();
    ErrorLevel = OKAY;

    Statement = Statement0;
    SQLHSTMT stmt = Statement->stat_handle;

    rc = SQLExecute(stmt);

    odbc_check(SQL_NULL_HANDLE, SQL_NULL_HANDLE, stmt, rc, &ErrorLevel, &Messages);
").


%----------------------------------------------------------------------------------------------------

:- pred get_row_count(
    int::out,
    odbc.statement::di, odbc.statement::uo,
    odbc.state::di, odbc.state::uo
) is det.

get_row_count(RowCount, !Statement, !DB) :-
    c_get_row_count(RowCount, !Statement, !DB, ErrorLevel, Messages),
    update_error_state(ErrorLevel, Messages, !DB).


:- pred c_get_row_count(
    int::out,
    odbc.statement::di, odbc.statement::uo,
    odbc.state::di, odbc.state::uo,
    error_level::out, messages::out
) is det.

:- pragma foreign_proc("C",
    c_get_row_count(
        RowCount::out,
        Statement0::di, Statement::uo, DB0::di, DB::uo,
        ErrorLevel::out, Messages::out),
    [ thread_safe, promise_pure, may_call_mercury, will_not_throw_exception ],
"
    SQLRETURN rc;
    SQLLEN rowcount;

    ErrorLevel = OKAY;
    Messages = MR_list_empty();

    Statement = Statement0;
    SQLHSTMT stmt = Statement->stat_handle;

    rc = SQLRowCount(stmt, &rowcount);
    odbc_check(SQL_NULL_HANDLE, SQL_NULL_HANDLE, stmt, rc, &ErrorLevel, &Messages);

    RowCount = rowcount;
    DB = DB0;
").


%----------------------------------------------------------------------------------------------------

% This cleans up the data associated with a statement and frees the ODBC statement handle (with SQLFreeHandle). In
% case the statement is a NULL pointer, nothing is done.

:- pred cleanup_statement(
    odbc.statement::di,
    odbc.state::di, odbc.state::uo
) is det.

cleanup_statement(Statement, !DB) :-
    !.DB = odbc.state(Env, Conn, _, _),
    unsafe_promise_unique(!.DB, !:DB),

    cleanup_statement_check_error(Env, Conn, Statement, !DB, ErrorLevel, Messages),
    update_error_state(ErrorLevel, Messages, !DB).


:- pred cleanup_statement_check_error(
    env::in, conn::in,
    odbc.statement::di,
    odbc.state::di, odbc.state::uo,
    error_level::out, messages::out
) is det.

:- pragma foreign_proc("C",
    cleanup_statement_check_error(
        Env::in, Conn::in,
        Statement::di,
        DB0::di, DB::uo,
        ErrorLevel::out, Messages::out),
    [ thread_safe, promise_pure, may_call_mercury, will_not_throw_exception ],
"{
    MODBC_Statement *statement;
    SQLRETURN rc;

    ErrorLevel = OKAY;
    Messages = MR_list_empty();

    statement = (MODBC_Statement *) Statement;

    rc = odbc_do_cleanup_statement(statement);
    odbc_check(Env, Conn, SQL_NULL_HSTMT, rc, &ErrorLevel, &Messages);

    DB = DB0;
}").


:- pragma foreign_code("C", "
/* This cleans up the data associated with the statement and frees the ODBC statement handle with SQLFreeHandle.
   It returns the return code of the SQLFreeHandle() call, or SQL_SUCCESS if the statement is NULL or the handle
   is SQL_NULL_HSTMT. The statement (a pointer to an MODBC_Statement structure) is freed and becomes invalid.
*/
SQLRETURN
odbc_do_cleanup_statement(MODBC_Statement *statement)
{
    int i;
    SQLRETURN rc;

    if (statement != NULL) {
        MR_DEBUG(printf(""cleaning up statement\\n""));
        if (statement->row != NULL) {
            for (i = 1; i <= statement->num_columns; i++) {
                // Variable length types are allocated directly
                // onto the Mercury heap, so don't free them here.
                if (!odbc_is_variable_length_sql_type(
                    statement->row[i].sql_type))
                {
                    MR_GC_free(statement->row[i].data);
                }
            }
            MR_GC_free(statement->row);
        }

        if (statement->stat_handle != SQL_NULL_HSTMT) {
            rc = SQLFreeHandle(SQL_HANDLE_STMT, statement->stat_handle);
        } else {
            rc = SQL_SUCCESS;
        }
        MR_GC_free(statement);
        return rc;
    } else {
        return SQL_SUCCESS;
    }
}
").

%----------------------------------------------------------------------------------------------------
% C data types/declarations and functions for statements and columns

% About memory allocation: See runtime/mercury_imp.h, line 112-

:- pragma foreign_type("C", statement, "MODBC_Statement *",
    [can_pass_as_mercury_type]).

:- pragma foreign_decl("C", "
// Notes on memory allocation:
//
// C data structures (MODBC_Statement and MODBC_Column) are allocated
// using MR_GC_malloc/MR_GC_free.
//
// MODBC_Statement contains a statement handle which must be freed
// using SQLFreeHandle.
//
// Variable length data types are collected in chunks allocated on
// the Mercury heap using MR_incr_hp_atomic. The chunks are then
// condensed into memory allocated on the Mercury heap using
// string.append_list.
// XXX this may need revisiting when accurate garbage collection
// is implemented to make sure the collector can see the data when
// it is stored within a MODBC_Column.
//
// Other data types have a buffer which is allocated once using
// MR_GC_malloc.

// If the driver can't work out how much data is in a blob in advance,
// get the data in chunks. The chunk size is fairly arbitrary.
// MODBC_CHUNK_SIZE must be a multiple of sizeof(MR_Word).

#define MODBC_CHUNK_WORDS   1024
#define MODBC_CHUNK_SIZE    (MODBC_CHUNK_WORDS * sizeof(MR_Word))

// We DON'T have an MODBC_INT type. We always return the exact integer type.
typedef enum {
    MODBC_FLOAT      = 1,   // Mercury Float
    MODBC_TIME       = 2,   // time and/or date converted to a string
    MODBC_STRING     = 3,   // string, or type converted to a string
    MODBC_VAR_STRING = 4,   // string with no maximum length
    MODBC_NULL       = 5,
    MODBC_INT64      = 6,
    MODBC_UINT64     = 7,
    MODBC_INT32      = 8,
    MODBC_UINT32     = 9,
    MODBC_INT16      = 10,
    MODBC_UINT16     = 11,
    MODBC_INT8       = 12,
    MODBC_UINT8      = 13,
    MODBC_BIT        = 14
} MODBC_AttrType;

typedef enum { MODBC_BIND_COL, MODBC_GET_DATA } MODBC_BindType;

// Information about a column in a result set.
typedef struct {
        size_t          size;       // size of allocated buffer
        MODBC_AttrType  attr_type;  // type of the attribute on the Mercury side
        int             is_unsigned;// for integer types, if it is unsinged
        SWORD           sql_type;   // the actual type, e.g. SQL_LONG_VAR_CHAR
        SWORD           conversion_type;
                                    // the type the data is being converted
                                    // into, e.g SQL_C_CHAR
        SQLLEN          value_info; // size of returned data, or SQL_NULL_DATA
        MR_Word         *data;
} MODBC_Column;

// Information about a result set.
typedef struct {
        SQLHSTMT        stat_handle;    // statement handle
        int             num_columns;    // columns per row
        MODBC_Column    *row;           // array of columns in the current row
        int             num_rows;       // number of fetched rows
        MODBC_BindType  binding_type;   // are we using SQL_BIND_COL
                                        // or SQL_GET_DATA
} MODBC_Statement;

// All floats get converted to double by the driver, then to MR_Float.
typedef long                    MODBC_C_INT;
typedef double                  MODBC_C_FLOAT;
typedef long long               MODBC_C_INT64;
typedef unsigned long long      MODBC_C_UINT64;
typedef int                     MODBC_C_INT32;
typedef unsigned int            MODBC_C_UINT32;
typedef short                   MODBC_C_INT16;
typedef unsigned short          MODBC_C_UINT16;
typedef char                    MODBC_C_INT8;
typedef unsigned char           MODBC_C_UINT8;

extern SQLRETURN
odbc_do_cleanup_statement(MODBC_Statement *statement);

extern size_t
odbc_sql_type_to_size(SWORD sql_type, UDWORD cbColDef, SWORD ibScale,
    SWORD fNullable);

extern MODBC_AttrType
odbc_sql_type_to_attribute_type(SWORD sql_type, int);

extern SWORD
odbc_attribute_type_to_sql_c_type(MODBC_AttrType AttrType);

extern MR_bool
odbc_is_variable_length_sql_type(SWORD);

extern int
odbc_do_get_data(SQLHENV, SQLHDBC, MODBC_Statement *statement, int column_id, MR_Word*, MR_Word*);

extern int
odbc_get_data_in_chunks(SQLHENV env, SQLHDBC conn, MODBC_Statement *statement, int column_id,
    MR_Word* error_level, MR_Word* messages);

extern int
odbc_get_data_in_one_go(
    SQLHENV env, SQLHDBC conn,
    MODBC_Statement *statement, int column_id,
    MR_Word* error_level, MR_Word* messages);
").



%----------------------------------------------------------------------------------------------------

:- pred bind_columns(
    odbc.statement::di, odbc.statement::uo,
    odbc.state::di, odbc.state::uo
) is det.

bind_columns(!Statement, !DB) :-
    !.DB = odbc.state(Env, Conn, _, _),
    unsafe_promise_unique(!.Statement, !:Statement),
    unsafe_promise_unique(!.DB, !:DB),
    c_bind_columns(Env, Conn, !Statement, !DB, ErrorLevel, Messages),
    update_error_state(ErrorLevel, Messages, !DB).


% There are two methods to get data back from an ODBC application.
%
% One involves binding a buffer to each column using SQLBindCol,
% then calling SQLFetch repeatedly to read rows into the buffers.
% The problem with this method is it doesn't work with variable
% length data, since if the data doesn't fit into the allocated
% buffer it gets truncated and there's no way to have a second
% try with a larger buffer.
%
% The other method is to not bind any columns. Instead, after
% SQLFetch is called to update the cursor, SQLGetData is used
% on each column to get the data. SQLGetData can be called repeatedly
% to get all the data if it doesn't fit in the buffer. The problem
% with this method is that it requires an extra ODBC function call
% for each attribute received, which may have a significant impact
% on performance if the database is being accessed over a network.
%
% Hybrid methods are also possible if all the variable length columns
% come after the fixed length columns in the result set, but that
% is probably overkill. (SQLGetData can only be used on columns
% after those bound using SQLBindCol).
%
% The first method is used if there are no variable length columns,
% otherwise the second method is used.
%
:- pred c_bind_columns(
    odbc.env::in, odbc.conn::in,
    odbc.statement::di, odbc.statement::uo,
    odbc.state::di, odbc.state::uo,
    ErrorLevel::out, Messages::out
) is det.

:- pragma foreign_proc("C",
    c_bind_columns(
        Env::in, Conn::in,
        Statement0::di, Statement::uo,
        DB0::di, DB::uo,
        ErrorLevel::out, Messages::out),
    [ thread_safe, promise_pure, may_call_mercury, will_not_throw_exception, may_not_duplicate ],
"
    int             column_no;
    SQLSMALLINT     num_columns;
    MODBC_Column    *column;
    SQLRETURN       rc;
    SQLHSTMT        stat_handle;

    Messages = MR_list_empty();
    ErrorLevel = OKAY;

    Statement = Statement0;
    stat_handle = Statement->stat_handle;

    // Retrieve the number of columns of the statement.
    rc = SQLNumResultCols(stat_handle, &num_columns);

    if (! odbc_check(Env, Conn, stat_handle, rc, &ErrorLevel, &Messages))
        goto c_bind_columns_end;
    Statement->num_columns = num_columns;

    // Allocate an array containing the info for each column.
    // The extra column is because ODBC counts columns starting from 1.
    Statement->row = MR_GC_NEW_ARRAY(MODBC_Column, num_columns + 1);

    // Use SQLBindCol unless there are columns with no set maximum length.
    Statement->binding_type = MODBC_BIND_COL;

    // Get information about the result set columns.
    // ODBC counts columns from 1.
    for (column_no = 1; column_no <= Statement->num_columns; column_no++) {
        char        col_name[1];    // Not looked at
        SWORD       col_name_len;
        SWORD       col_type;
        SQLULEN     pcbColDef;
        SWORD       pibScale;
        SWORD       pfNullable;

        column = &(Statement->row[column_no]);
        column->size = 0;
        column->data = NULL;

        // Retrieve the C type of the column.
        // (SQL type mapped to a conversion type).
        // Create an attribute object with room to store the
        // attribute value.
        rc = SQLDescribeCol(stat_handle, column_no,
                (UCHAR *) col_name, sizeof(col_name),
                &col_name_len, &col_type, &pcbColDef,
                &pibScale, &pfNullable);

        // SQL_SUCCESS_WITH_INFO means there wasn't
        // enough space for the column name, but we
        // aren't collecting the column name anyway.
        if (rc != SQL_SUCCESS_WITH_INFO &&
            ! odbc_check(Env, Conn, stat_handle, rc, &ErrorLevel, &Messages))
        {
            rc = odbc_do_cleanup_statement(Statement);
            Statement = NULL;
            odbc_check(Env, Conn, SQL_NULL_HSTMT, rc, &ErrorLevel, &Messages);
            goto c_bind_columns_end;
        }

        column->sql_type = col_type;
        column->size = odbc_sql_type_to_size(col_type, pcbColDef,
            pibScale, pfNullable);

        // For an integer type, determine if it is unsigned (SQL_TRUE or SQL_FALSE). For attributes which aren't
        // integers, this returns SQL_FALSE.

        SQLLEN is_unsigned = 0;
        rc = SQLColAttribute(stat_handle, column_no, SQL_DESC_UNSIGNED, NULL, 0, NULL, &is_unsigned);

        if (! odbc_check(Env, Conn, stat_handle, rc, &ErrorLevel, &Messages))
        {
            rc = odbc_do_cleanup_statement(Statement);
            odbc_check(Env, Conn, SQL_NULL_HSTMT, rc, &ErrorLevel, &Messages);
            Statement = NULL;
            goto c_bind_columns_end;
        }
        column->is_unsigned = is_unsigned;



        column->attr_type = odbc_sql_type_to_attribute_type(col_type, is_unsigned);

        // Request a conversion into one of the supported types.
        column->conversion_type =
            odbc_attribute_type_to_sql_c_type(column->attr_type);

        MR_DEBUG(printf(""Column %i: size %i - sql_type %i - attr_type %i - conversion_type %i\\n"",
            column_no, column->size, column->sql_type,
            column->attr_type, column->conversion_type));

        if (odbc_is_variable_length_sql_type(col_type)) {
            Statement->binding_type = MODBC_GET_DATA;
        } else {
            // Do the buffer allocation once for columns which have
            // a fixed maximum length.
            column->data = MR_GC_malloc(column->size);
        }

    } // for

    if (Statement->binding_type == MODBC_BIND_COL) {
        for (column_no = 1; column_no <= Statement->num_columns; column_no++) {
            MR_DEBUG(printf(""Binding column %d/%d\\n"", column_no, Statement->num_columns));

            column = &(Statement->row[column_no]);

            rc = SQLBindCol(stat_handle, column_no,
                column->conversion_type, (SQLPOINTER) column->data,
                column->size, &(column->value_info));

            if (! odbc_check(Env, Conn, stat_handle, rc, &ErrorLevel, &Messages))
            {
                odbc_do_cleanup_statement(Statement);
                Statement = NULL;
                goto c_bind_columns_end;
            }
        }
    }

    c_bind_columns_end:
    DB = DB0;
").


%----------------------------------------------------------------------------------------------------

% Get the number of columns of the last row, which is stored inside of the MODBC_Statement structure. (A statement
% is a pointer to this structure.) This always succeeds.

:- pred get_number_of_columns(
    int::out,
    odbc.statement::di, odbc.statement::uo,
    odbc.state::di, odbc.state::uo
) is det.

:- pragma foreign_proc("C",
    get_number_of_columns(NumColumns::out, Statement0::di, Statement::uo,
        DB0::di, DB::uo),
    [ thread_safe, promise_pure, will_not_call_mercury, will_not_throw_exception],
    "
    Statement = Statement0;
    MR_assert(Statement != NULL);
    NumColumns = Statement->num_columns;
    DB = DB0;
    ").


%----------------------------------------------------------------------------------------------------

    % Get the set of result rows from the statement.
    %
:- pred get_rows(list(odbc.row), odbc.statement, odbc.statement, odbc.state, odbc.state).
:- mode get_rows(out, di, uo, di, uo) is det.

get_rows(Rows, !Statement, !DB) :-
    get_number_of_columns(NumColumns, !Statement, !DB),
    chain(get_rows_2(NumColumns, []), Rows, [], !Statement, !DB).


:- pred get_rows_2(int, list(odbc.row), list(odbc.row), odbc.statement, odbc.statement, odbc.state, odbc.state).
:- mode get_rows_2(in, in, out, di, uo, di, uo) is det.

get_rows_2(NumColumns, !Result, !Statement, !DB) :-

    % Try to fetch a new row. This cleans up the statement in case of an error, and makes the statement NULL. This
    % in turn is checked in cleanup_statement.
    fetch_row(Status, !Statement, !DB),

    ( if no_data(Status) then
        true
    else
        chain(get_attributes(1, NumColumns), Row, [], !Statement, !DB),
        !:Result = [Row | !.Result],
        chain(get_rows_2(NumColumns, !.Result), !:Result, [], !Statement, !DB)
    ).


:- pred odbc.no_data(int::in) is semidet.

:- pragma foreign_proc("C",
    odbc.no_data(Status::in),
    [ thread_safe, promise_pure, will_not_call_mercury, will_not_throw_exception ],
"
    SUCCESS_INDICATOR = (Status == SQL_NO_DATA_FOUND);
").


%----------------------------------------------------------------------------------------------------


% Fetch a row from the given statement handle and save it in the statement. get_attributes is used to get the row.
% Returns the status from the SQLFetch() call. This status can be SQL_NO_DATA_FOUND, which is determined by the
% no_data/1 predicate. In case of an error, the statement (a pointer to an MODBC_Statement structure) is cleaned up
% and becomes invalid. This means that the odbc.state needs to be examined after the call.

:- pred fetch_row(
    int::out,                                   % Status
    odbc.statement::di, odbc.statement::uo,     % Statement state (pointer to an MODBC_Statement structure)
    odbc.state::di, odbc.state::uo              % Database state
) is det.

fetch_row(Status, !Statement, !DB) :-
    !.DB = odbc.state(Env, Conn, _, _),
    unsafe_promise_unique(!.DB, !:DB),
    c_fetch_row(Env, Conn, !Statement, Status, !DB, ErrorLevel, Messages),
    update_error_state(ErrorLevel, Messages, !DB).


% Fetch the next row of the current statement. Returns the status from the SQLFetch() call. This status can be
% SQL_NO_DATA_FOUND, which is determined by the no_data/1 predicate. In case of an error, the statement is cleaned
% up and becomes invalid.

:- pred c_fetch_row(
    odbc.env::in, odbc.conn::in,
    odbc.statement::di, odbc.statement::uo,
    int::out,
    odbc.state::di, odbc.state::uo,
    error_level::out, messages::out
) is det.

:- pragma foreign_proc("C",
    c_fetch_row(Env::in, Conn::in,
                Statement0::di, Statement::uo,
                Status::out,
                DB0::di, DB::uo,
                ErrorLevel::out, Messages::out),
    [ thread_safe, promise_pure, may_call_mercury, will_not_throw_exception, may_not_duplicate ],
"
    Statement = Statement0;
    MR_assert(Statement != NULL);

    if (Statement->num_rows == 0) {
        MR_DEBUG(printf(""Fetching rows...\\n""));
    }

    ErrorLevel = OKAY;
    Messages = MR_list_empty();

    // Fetching new row
    Status = SQLFetch(Statement->stat_handle);

    if (Status != SQL_NO_DATA_FOUND &&
        ! odbc_check(Env, Conn, Statement->stat_handle, Status, &ErrorLevel, &Messages))
    {
        odbc_do_cleanup_statement(Statement);
        Statement = NULL;
        goto c_fetch_row_end;
    }

    // Update number of rows fetched.
    if (Status == SQL_SUCCESS) {
        Statement->num_rows++;
    }

    if (Status == SQL_NO_DATA_FOUND) {
        MR_DEBUG(printf(""Fetched %d rows\\n"", Statement->num_rows));
    }

    c_fetch_row_end:
    DB = DB0;
").


%----------------------------------------------------------------------------------------------------

    % Get the values from the current fetched row.
    %
:- pred get_attributes(
    int::in, int::in,
    list(odbc.attribute)::out,
    odbc.statement::di, odbc.statement::uo,
    odbc.state::di, odbc.state::uo
) is det.

get_attributes(CurrCol, NumCols, Row, !Statement, !DB) :-
    ( if CurrCol =< NumCols then
        NextCol = CurrCol + 1,
        chain(get_attribute(CurrCol), Attribute, null, !Statement, !DB),
        chain(get_attributes(NextCol, NumCols), Row1, [], !Statement, !DB),
        Row = [Attribute | Row1]
    else
        Row = []
    ).


%----------------------------------------------------------------------------------------------------

    % Get the value of a column in the current fetched row.
    %
:- pred get_attribute(
    int::in, odbc.attribute::out,
    odbc.statement::di, odbc.statement::uo,
    odbc.state::di, odbc.state::uo
) is det.

get_attribute(NumColumn, Value, !Statement, !DB) :-
    !.DB = odbc.state(Env, Conn, _, _),
    unsafe_promise_unique(!.DB, !:DB),
    get_data(Env, Conn, NumColumn,
        Int64, Uint64, Int32, Uint32, Int16, Uint16, Int8, Uint8, Float, String,
        TypeInt, !Statement, !DB, ErrorLevel, Messages),
    update_error_state(ErrorLevel, Messages, !DB),

    int_to_attribute_type(TypeInt, Type),
    (
        Type = null,
        Value = null
    ;
        Type = string,
        Value = string(String)
    ;
        Type = time,
        Value = time(String)
    % ;
    %     Type = int,
    %     Value = int(Int)
    ;
        Type = float,
        Value = float(Float)
    ;
        Type = int64,
        Value = int64(Int64)
    ;
        Type = uint64,
        Value = uint64(Uint64)
    ;
        Type = int32,
        Value = int32(Int32)
    ;
        Type = uint32,
        Value = uint32(Uint32)
    ;
        Type = int16,
        Value = int16(Int16)
    ;
        Type = uint16,
        Value = uint16(Uint16)
    ;
        Type = int8,
        Value = int8(Int8)
    ;
        Type = uint8,
        Value = uint8(Uint8)
    ).


%----------------------------------------------------------------------------------------------------

:- type attribute_type
    --->    float
    ;       time
    ;       string
    ;       null
    ;       int64
    ;       uint64
    ;       int32
    ;       uint32
    ;       int16
    ;       uint16
    ;       int8
    ;       uint8.

:- pred int_to_attribute_type(int::in, odbc.attribute_type::out) is det.

int_to_attribute_type(Int, Type) :-
    ( if int_to_attribute_type_2(Int, Type1) then
        Type = Type1
    else
        error("odbc.int_to_attribute_type: invalid type")
    ).

    % Keep this in sync with the C enum MODBC_AttrType.
    %
:- pred int_to_attribute_type_2(int::in, odbc.attribute_type::out) is semidet.

int_to_attribute_type_2(1, float).
int_to_attribute_type_2(2, time).
int_to_attribute_type_2(3, string).
int_to_attribute_type_2(4, string).
int_to_attribute_type_2(5, null).
int_to_attribute_type_2(6, int64).
int_to_attribute_type_2(7, uint64).
int_to_attribute_type_2(8, int32).
int_to_attribute_type_2(9,  uint32).
int_to_attribute_type_2(10, int16).
int_to_attribute_type_2(11, uint16).
int_to_attribute_type_2(12, int8).
int_to_attribute_type_2(13, uint8).
int_to_attribute_type_2(14, uint8).


%----------------------------------------------------------------------------------------------------

:- pred get_data(
    env::in, conn::in,
    int::in,
    int64::out, uint64::out, int32::out, uint32::out, int16::out, uint16::out, int8::out, uint8::out,
    float::out, string::out,
    int::out,
    odbc.statement::di, odbc.statement::uo, odbc.state::di, odbc.state::uo,
    error_level::out, messages::out
) is det.

:- pragma foreign_proc("C",
    get_data(
        Env::in, Conn::in,
        Column::in,
        Int64::out, Uint64::out, Int32::out, Uint32::out, Int16::out, Uint16::out, Int8::out, Uint8::out,
        Flt::out, Str::out,
        Type::out,
        Statement0::di, Statement::uo, DB0::di, DB::uo,
        ErrorLevel::out, Messages::out
    ),
    [ thread_safe, promise_pure, may_call_mercury, will_not_throw_exception, may_not_duplicate ],
"
    MODBC_Column    *col;
    SQLRETURN       rc;
    SDWORD          column_info;

    Statement = Statement0;

    MR_assert(Statement != NULL);
    MR_assert(Statement->row != NULL);

    MR_DEBUG(printf(""Getting column %i\\n"", (int) Column));

    ErrorLevel = OKAY;
    Messages = MR_list_empty();

    if (Statement->binding_type == MODBC_GET_DATA) {
        // Slurp up the data for this column.
        if (odbc_do_get_data(Env, Conn, Statement, Column, &ErrorLevel, &Messages))
            goto get_data_end;
    }

    col = &(Statement->row[Column]);

    if (col->value_info == SQL_NULL_DATA) {
        Type = MODBC_NULL;
    } else {
        Type = col->attr_type;
    }

    switch ((int) Type) {
        case MODBC_NULL:
            break;

        case MODBC_INT64:
            Int64 = *(MODBC_C_INT64 *)(col->data);
            break;

        case MODBC_UINT64:
            Uint64 = *(MODBC_C_UINT64 *)(col->data);
            break;

        case MODBC_INT32:
            Int32 = *(MODBC_C_INT32 *)(col->data);
            break;

        case MODBC_UINT32:
            Uint32 = *(MODBC_C_UINT32 *)(col->data);
            break;

        case MODBC_INT16:
            Int16 = *(MODBC_C_INT16 *)(col->data);
            break;

        case MODBC_UINT16:
            Uint16 = *(MODBC_C_UINT16 *)(col->data);
            break;

        case MODBC_INT8:
            Int8 = *(MODBC_C_INT8 *)(col->data);
            break;

        case MODBC_UINT8:
            Uint8 = *(MODBC_C_UINT8 *)(col->data);
            break;

        case MODBC_FLOAT:
            Flt = (MR_Float) *(MODBC_C_FLOAT *)(col->data);

            MR_DEBUG(printf(""got float %f\\n"", Flt));

            break;

        case MODBC_STRING:
        case MODBC_TIME:
            MR_assert(col->data);
            MR_make_aligned_string_copy(Str, (char *) col->data);

            MR_DEBUG(printf(""got string %s\\n"", (char *) Str));

            break;

        case MODBC_VAR_STRING:
            // The data was allocated on the Mercury heap,
            // get it then kill the pointer so it can be GC'ed.
            MR_make_aligned_string(Str, (char *) col->data);

            MR_DEBUG(printf(""got var string %s\\n"", (char *) col->data));

            col->data = NULL;

            // As far as Mercury is concerned it's an ordinary string.
            Type = MODBC_STRING;
            break;

        default:
            MR_external_fatal_error(""odbc.m"", ""invalid attribute type in odbc.get_data"");
            break;
    } // end switch (Type)

    get_data_end:
    DB = DB0;
").


%----------------------------------------------------------------------------------------------------


:- pragma foreign_code("C", "

// Returns if the operation should be canceled, because of an ODBC error. The caller is to return, when it has
// gotten a TRUE (1).

int odbc_do_get_data(
    SQLHENV env, SQLHDBC conn,
    MODBC_Statement *statement, int column_id,
    MR_Word* error_level, MR_Word* messages)
{
    MODBC_Column    *column;
    SQLRETURN       rc;
    SDWORD          column_info;
    char            dummy_buffer[1]; // Room for the NUL termination byte
                                     // and nothing else.

    column = &(statement->row[column_id]);
    if (column->attr_type == MODBC_VAR_STRING) {
        // Just get the length first time through.
        rc = SQLGetData(statement->stat_handle, column_id,
            column->conversion_type, dummy_buffer,
            1, &(column->value_info));

        // SQL_SUCCESS_WITH_INFO is expected here, since we didn't allocate
        // any space for the data, so don't collect the ""data truncated""
        // message.
        if (rc != SQL_SUCCESS_WITH_INFO &&
            ! odbc_check(env, conn, statement->stat_handle, rc, error_level, messages))
        {
            odbc_do_cleanup_statement(statement);
            return 1;
        }

        if (column->value_info == SQL_NULL_DATA) {
            // The column is NULL, so there is no data to get.
            return 0;
        } else if (column->value_info == SQL_NO_TOTAL) {
            // The driver couldn't work out the length in advance, so
            // get the data in chunks of some arbitrary size, and append
            // the chunks together.
            // This method must be used with MODBC_IODBC,
            // since iODBC-2.12 uses a different interpretation
            // of the ODBC standard to Microsoft, for which
            // the length returned by the first call to SQLGetData
            // above is the minimum of the buffer length and the
            // length of the available data, rather than the
            // total length of data available.

            if (odbc_get_data_in_chunks(env, conn, statement, column_id, error_level, messages))
                return 1;
        } else {
            MR_Word data;

            // column->value_info == length of data
            column->size = column->value_info + 1;
            MR_incr_hp_atomic(data,
                (column->size + sizeof(MR_Word)) / sizeof(MR_Word));
            column->data = (MR_Word *) data;

            if (odbc_get_data_in_one_go(env, conn, statement, column_id, error_level, messages))
                return 1;
        }
    } else {
        // It's a fixed length column, so we can get the lot in one go.
        if (odbc_get_data_in_one_go(env, conn, statement, column_id, error_level, messages))
            return 1;
    }

    return 0;
}


// Returns if the operation should be canceled, because of an ODBC error. The caller is to return, when it has
// gotten a TRUE (1).

int
odbc_get_data_in_chunks(
    SQLHENV env, SQLHDBC conn,
    MODBC_Statement *statement, int column_id,
    MR_Word* error_level, MR_Word* messages)
{
    MODBC_Column    *col;
    SQLRETURN       rc;
    MR_Word         this_bit;
    MR_Word         chunk_list;
    MR_String       result;

    MR_DEBUG(printf(""getting column %i in chunks\\n"", column_id));

    chunk_list = MR_list_empty();

    col = &(statement->row[column_id]);

    rc = SQL_SUCCESS_WITH_INFO;

    MR_incr_hp_atomic(this_bit, MODBC_CHUNK_WORDS);

    // Keep collecting chunks until we run out.
    while (rc == SQL_SUCCESS_WITH_INFO) {
        rc = SQLGetData(statement->stat_handle, column_id,
            col->conversion_type, (SQLPOINTER) this_bit,
            MODBC_CHUNK_SIZE - 1, &(col->value_info));

        if (rc == SQL_NO_DATA_FOUND) {
            break;
        }

        if (rc != SQL_SUCCESS_WITH_INFO &&
            ! odbc_check(env, conn, statement->stat_handle, rc, error_level, messages))
        {
            odbc_do_cleanup_statement(statement);
            return 1;
        }

        chunk_list = MR_list_cons(this_bit, chunk_list);
        MR_incr_hp_atomic(this_bit, MODBC_CHUNK_WORDS);
    }

    MODBC_odbc_condense_chunks(chunk_list, &result);
    col->data = (MR_Word *) result;

    return 0;
}


// Returns if the operation should be canceled, because of an ODBC error. The caller is to return, when it has
// gotten a TRUE (1).

int
odbc_get_data_in_one_go(
    SQLHENV env, SQLHDBC conn,
    MODBC_Statement *statement, int column_id,
    MR_Word* error_level, MR_Word* messages)
{
    MODBC_Column    *col;
    SQLRETURN       rc;

    MR_DEBUG(printf(""getting column %i in one go\\n"", column_id));

    col = &(statement->row[column_id]);

    rc = SQLGetData(statement->stat_handle, column_id,
        col->conversion_type,
        (SQLPOINTER) col->data, col->size, &(col->value_info));

    if (! odbc_check(env, conn, statement->stat_handle, rc, error_level, messages))
    {
        odbc_do_cleanup_statement(statement);
        return 1;
    }

    return 0;
}
").


:- pragma foreign_export("C", condense_chunks(in, out),
    "MODBC_odbc_condense_chunks").
:- pred condense_chunks(list(string)::in, string::out) is det.

condense_chunks(RevChunks, String) :-
    list.reverse(RevChunks, Chunks),
    string.append_list(Chunks, String).



%----------------------------------------------------------------------------------------------------
% 7. Catalog functions
%----------------------------------------------------------------------------------------------------

data_sources(environment(Env), Result, !IO) :-

    sql_data_sources(Env, RevSourceNames, RevDescs, Status, ErrorLevel, RevMessages, !IO),

    ( if odbc.ok(Status) then
        list.reverse(RevMessages, Messages),
        list.reverse(RevSourceNames, SourceNames),
        list.reverse(RevDescs, Descs),
        assoc_list.from_corresponding_lists(SourceNames, Descs, SourceAL),
        MakeSource =
            ( pred(Pair::in, SourceDesc::out) is det :-
                Pair = SourceName - Desc,
                SourceDesc = odbc.source_desc(SourceName, Desc)
            ),
        list.map(MakeSource, SourceAL, Sources),
        make_result(Sources, ErrorLevel, Messages, Result)

    else if odbc.no_data(Status) then
        % iODBC 2.12 doesn't implement this function.
        Messages = [
            error(feature_not_implemented) -
            "[Mercury][odbc.m]SQLDataSources not implemented."
        ],
        Result = odbc.error(Messages)
    else
        list.reverse(RevMessages, Messages),
        Result = odbc.error(Messages)
    ).

%----------------------------------------------------------------------------------------------------

:- pred sql_data_sources(
    env::in, list(string)::out, list(string)::out, int::out,
    error_level::out, messages::out, io::di, io::uo
) is det.

:- pragma foreign_proc("C",
    sql_data_sources(
        Env::in, SourceNames::out, SourceDescs::out, Status::out,
        ErrorLevel::out, Messages::out,
        IO0::di, IO::uo),
    [ promise_pure, thread_safe, may_call_mercury, will_not_throw_exception ],
"
    ErrorLevel = OKAY;
    Messages = MR_list_empty();
    Status = odbc_do_get_data_sources(Env, &SourceNames, &SourceDescs, &ErrorLevel, &Messages);
    IO = IO0;
").

:- pragma foreign_decl("C", "
SQLRETURN
odbc_do_get_data_sources(
    SQLHENV, MR_Word *SourceNames, MR_Word *SourceDescs,
    MR_Word* ErrorLevel, MR_Word *Messages);
").

:- pragma foreign_code("C", "
SQLRETURN
odbc_do_get_data_sources(
    SQLHENV odbc_env_handle,
    MR_Word *SourceNames, MR_Word *SourceDescs,
    MR_Word* ErrorLevel, MR_Word *odbc_message_list)
{
    SQLCHAR dsn[SQL_MAX_DSN_LENGTH];
    SQLCHAR desc[128];

    // Arbitrary size, only needs to hold a
    // descriptive string like ""SQL Server"".

    MR_String new_dsn;
    MR_String new_desc;
    SWORD dsn_len;
    SWORD desc_len;
    SQLRETURN rc;

    *SourceNames = MR_list_empty();
    *SourceDescs = MR_list_empty();

    if (odbc_env_handle == SQL_NULL_HENV) {
        rc = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &odbc_env_handle);
    } else {
        rc = SQL_SUCCESS;
    }

    MR_DEBUG(printf(""SQLAllocEnv status: %d\\n"", rc));

    if (odbc_check(odbc_env_handle, SQL_NULL_HDBC, SQL_NULL_HSTMT, rc, ErrorLevel, odbc_message_list)) {
        rc = SQLDataSources(odbc_env_handle, SQL_FETCH_FIRST,
            dsn, SQL_MAX_DSN_LENGTH - 1, &dsn_len,
            desc, sizeof(desc), &desc_len);

        // The documentation varies on whether the driver
        // returns SQL_NO_DATA_FOUND or SQL_NO_DATA, so check for both.
        while (rc != SQL_NO_DATA_FOUND && rc != SQL_NO_DATA &&
            odbc_check(odbc_env_handle, SQL_NULL_HDBC, SQL_NULL_HSTMT, rc, ErrorLevel, odbc_message_list))
        {
            // Copy the new data onto the Mercury heap
            MR_make_aligned_string_copy(new_dsn, (MR_String)dsn);
            *SourceNames = MR_list_cons((MR_Word)new_dsn, *SourceNames);
            MR_make_aligned_string_copy(new_desc, (MR_String)desc);
            *SourceDescs = MR_list_cons((MR_Word)new_desc, *SourceDescs);

            rc = SQLDataSources(odbc_env_handle,
                SQL_FETCH_NEXT, dsn, SQL_MAX_DSN_LENGTH - 1, &dsn_len,
                desc, sizeof(desc), &desc_len);
        }
    }

    if (rc == SQL_NO_DATA_FOUND) {
        rc = SQL_SUCCESS;
    }

    return rc;
}").


%----------------------------------------------------------------------------------------------------

tables(QualifierStr, OwnerStr, TableNameStr, Result, !DB) :-
    unsafe_promise_unique(!.DB, !:DB),
    tables_solutions(QualifierStr, OwnerStr, TableNameStr, Result, !DB).


% This is a variant of the solutions/6 predicate. It calls SQLTables and receives the result set as a list of
% table_desc. (The table descriptions are returned as an ordinary result set.) The result rows are converted to
% values of type table_desc.

:- pred tables_solutions(
    string::in,                % Qualifier pattern
    string::in,                % Owner pattern
    string::in,                % Table name pattern
    odbc.result(list(odbc.table_desc))::out,
    odbc.state::di, odbc.state::uo
) is det.

tables_solutions(QualifierStr, OwnerStr, TableStr, Result, !DB) :-
    some [!Statement] (
        !.DB = odbc.state(Env, Conn, _, _),
        alloc_statement(Env, Conn, !:Statement, !:DB),

        chain(sql_tables(Env, Conn, QualifierStr, OwnerStr, TableStr),
              !Statement,
              !DB),
        chain(bind_columns,           !Statement, !DB),
        chain(get_rows, RowsRev, [],  !Statement, !DB),
        cleanup_statement(!.Statement, !DB),

        (
           if
               list.map(convert_table_desc, reverse(RowsRev), Tables)
           then
               !.DB = odbc.state(_, _, ErrorLevel, Messages),
               unsafe_promise_unique(!.DB, !:DB),
               make_result(Tables, ErrorLevel, Messages, Result)
           else
               update_error_state(
                   errors,
                   [ odbc.error(internal_error) - "[Mercury][odbc.m]Invalid results from SQLTables." ],
                   !DB),
               !.DB = odbc.state(_, _, _, Messages1),
               Result = odbc.error(reverse(Messages1)),
               unsafe_promise_unique(!.DB, !:DB)
        )
    ).


:- pred sql_tables(
    env::in, conn::in,                          % Environment and connection handles
    string::in,                                 % Qualifier (catalog name) pattern
    string::in,                                 % Owner pattern
    string::in,                                 % Table name pattern
    odbc.statement::di, odbc.statement::uo,     % !Statement
    odbc.state::di, odbc.state::uo              % !DB
) is det.

sql_tables(
    Env, Conn,
    QualifierStr, OwnerStr, TableStr,
    !Statement, !DB
) :-
    c_sql_tables(
        Env, Conn,
        QualifierStr, OwnerStr, TableStr,
        ErrorLevel, Messages,
        !Statement, !DB),
    update_error_state(ErrorLevel, Messages, !DB).


:- pred c_sql_tables(
    env::in, conn::in,                          % Environment and connection handles
    string::in,                                 % Qualifier (catalog name) pattern
    string::in,                                 % Owner pattern
    string::in,                                 % Table name pattern
    error_level::out, messages::out,            % Error handling
    odbc.statement::di, odbc.statement::uo,     % !Statement
    odbc.state::di, odbc.state::uo              % !DB
) is det.

:- pragma foreign_proc("C",
    c_sql_tables(
        Env::in, Conn::in,
        QualifierStr::in, OwnerStr::in, TableStr::in,
        ErrorLevel::out, Messages::out,
        Statement0::di, Statement::uo,
        DB0::di, DB::uo),
    [ thread_safe, may_call_mercury, promise_pure, will_not_throw_exception ],
"
    ErrorLevel = OKAY;
    Messages = MR_list_empty();

    char *qualifier_str = NULL;
    char *owner_str = NULL;
    char *table_str = NULL;
    int qualifier_len = 0;
    int owner_len = 0;
    int table_len = 0;
    SQLRETURN rc;

    Statement = Statement0;

    // A NULL pointer in any of the string pattern fields
    // means no constraint on the search for that field.

    if (*QualifierStr) {
        qualifier_str = (char *) QualifierStr;
        qualifier_len = strlen(qualifier_str);
    }
    if (*OwnerStr) {
        owner_str = (char *) OwnerStr;
        owner_len = strlen(owner_str);
    }
    if (*TableStr) {
        table_str = (char *) TableStr;
        table_len = strlen(table_str);
    }

    rc = SQLTables(
        Statement->stat_handle,
        (SQLCHAR *)qualifier_str, qualifier_len,
        (SQLCHAR *)owner_str, owner_len,
        (SQLCHAR *)table_str, table_len,
        NULL, 0);
    odbc_check(Env, Conn, Statement->stat_handle, rc, &ErrorLevel, &Messages);

    DB = DB0;
").



% Convert a result row of the SQLTables call to a table_desc value.

:- pred convert_table_desc(odbc.row::in, odbc.table_desc::out) is semidet.

convert_table_desc(Row0, Table) :-
    NullToEmptyStr =
        ( pred(Data0::in, Data::out) is det :-
            ( if Data0 = null then Data = string("") else Data = Data0 )
        ),
    list.map(NullToEmptyStr, Row0, Row),
    Row = [ string(Qualifier), string(Owner), string(Name), string(Type), string(Description) | DriverColumns],
    Table = odbc.table_desc(Qualifier, Owner, Name, Type, Description, DriverColumns).


%----------------------------------------------------------------------------------------------------
% 8. Error handling
%----------------------------------------------------------------------------------------------------

%----------------------------------------------------------------------------------------------------
% odbc_check

:- pragma foreign_decl("C", "
extern int
odbc_check(SQLHENV sqlhenv, SQLHDBC sqlhdbc, SQLHSTMT sqlhstmt,
    SQLRETURN rc,
    MR_Word* error, MR_Word* odbc_message_list);

extern int
odbc_check_one(
    SQLSMALLINT handletype, SQLHANDLE handle, SQLSMALLINT recnumber, MR_Word* messages);
").



:- pragma foreign_code("C",
"

/* The odbc_check function handles ODBC errors in the C code. In the Mercury code, this is accomplished by the two
   ""chain"" predicates.

   odbc_check() keeps track of ODBC errors. It checks and updates the error level (in the error_level parameter)
   and the messages (in the messages parameter). It returns if it is okay to continue, meaning there were only ODBC
   calls which signaled success, by returning either SQL_SUCCESS or SQL_SUCCESS_WITH_INFO. If the latest ODBC call,
   whose return code is supplied in the ""rc"" argument to odbc_check(), is a failure (not SQL_SUCCESS or
   SQL_SUCCESS_WITH_INFO), then cleanup is to be done and execution must be aborted. odbc_check() will return 0 in
   this case.

   This means that continuation is okay as long as the error level is ""OKAY"" or ""WARNINGS"".

   The error level and the messages are must be initialised with OKAY and an empty list, before odbc_check() is
   called. This is done in the foreign procedures, which are defined by "":- pragma foreign_proc(...)"".

   Three handles are passed to odbc_check(). Each can be SQL_NULL_HANDLE, then it is ignored. The function can't
   cope with invalid handles. Handles must be valid (or SQL_NULL_HANDLE in order to ignore it).
*/

int odbc_check(
    SQLHENV sqlhenv, SQLHDBC sqlhdbc, SQLHSTMT sqlhstmt,
    SQLRETURN rc,
    MR_Word* error_level, MR_Word* messages)
{
    SQLSMALLINT recnumber;

    if (rc == SQL_SUCCESS) {
        /* No error level or messages to update. */
        return 1;

    } else {

        if (rc == SQL_SUCCESS_WITH_INFO) {
            *error_level = max(*error_level, WARNINGS);
        } else {
            // The return code in rc signals an error.
            *error_level = ERRORS;
        }

        // For each of the three handles, which are handed over to odbc_check(), get the associated messages
        // (warning and error messages) and add them to the messages list.

        for (recnumber = 1;
             odbc_check_one(SQL_HANDLE_ENV, sqlhenv, recnumber, messages);
             recnumber++);

        for (recnumber = 1;
             odbc_check_one(SQL_HANDLE_DBC, sqlhdbc, recnumber, messages);
             recnumber++);

        for (recnumber = 1;
             odbc_check_one(SQL_HANDLE_STMT, sqlhstmt, recnumber, messages);
             recnumber++);


        // We singal to continue execution when there are no errors. Warnings are allowed.

        if (*error_level == OKAY || *error_level == WARNINGS) {
            return 1;
        } else {
            return 0;
        }
    }
}


// Get the (zero or one) message for one handle and one record number. Returns if there might still be more. Adds
// the message to the messages list (if any).

int odbc_check_one(
    SQLSMALLINT handletype,    /* ODBC handle type */
    SQLHANDLE   handle,        /* ODBC handle */
    SQLSMALLINT recnumber,     /* Record number (for SQLGetDiagRec) */
    MR_Word*    messages)      /* Message list (reverse order) to which the messages will be added */
{
    SQLRETURN status;

    if (handle != SQL_NULL_HANDLE) {

       SQLSMALLINT textlength;
       SQLCHAR     sqlstate[6];
       SQLINTEGER  native_error;

       /* Determine the length of the message text. */
       status = SQLGetDiagRec(
           handletype, handle,
           recnumber, sqlstate, &native_error,
           NULL, 1, &textlength);

       if (status == SQL_SUCCESS || status == SQL_SUCCESS_WITH_INFO) {

           /* Allocate enough space for the message text and do it again. */
           SQLCHAR messagetext[textlength + 1];
           status = SQLGetDiagRec(
               handletype, handle,
               recnumber, sqlstate, &native_error,
               messagetext, textlength + 1, &textlength);

           /* Copy the ODBC SQL state and the ODBC message text to the Mercury heap. */
           MR_String m_sqlstate = (MR_String) MR_malloc(sizeof(sqlstate) + 1);
           strcpy((char*) m_sqlstate, (char*) sqlstate);

           MR_String m_messagetext = (MR_String) MR_malloc(textlength + 1);
           strcpy((char*) m_messagetext, (char*) messagetext);

           /* Convert the SQL state to a message. */
           MR_Word new_message;
           MODBC_odbc_sql_state_to_message((MR_String) sqlstate, m_messagetext, &new_message);

           /* Append the message onto the list. */
           *messages = MR_list_cons(new_message, *messages);
       }

       if (status == SQL_SUCCESS) {
           return 1;
       } else {
           return 0;
       }

    } else {
        /* null handle */
        return 0;
    }
}
").


%----------------------------------------------------------------------------------------------------

    % Handle ODBC error codes. Refer to the ODBC API Reference
    % provided with the ODBC SDK. The first two characters of the
    % SQLSTATE are meant to specify an error class. Looking at the
    % predicates below, the classes weren't terribly well chosen.
    %
:- pred sql_state_to_message(string::in, string::in, odbc.message::out) is det.

:- pragma foreign_export("C", sql_state_to_message(in, in, out),
    "MODBC_odbc_sql_state_to_message").

sql_state_to_message(SQLState, String, Message - String) :-
    string.split(SQLState, 2, Class, SubClass),
    ( Class = "01" ->
        ( sql_state_to_warning(SubClass, Warning) ->
            Message = warning(Warning)
        ;
            Message = warning(general_warning)
        )
    ;
        ( sql_state_to_error(Class, SubClass, Error) ->
            Message = error(Error)
        ;
            Message = error(general_error)
        )
    ).

:- pred sql_state_to_warning(string::in, odbc.warning::out) is semidet.

sql_state_to_warning("000", general_warning).
sql_state_to_warning("001", general_warning).
sql_state_to_warning("002", disconnect_error).
sql_state_to_warning("003", null_value_in_set_function).
sql_state_to_warning("004", string_data_truncated).
sql_state_to_warning("006", privilege_not_revoked).
sql_state_to_warning("007", privilege_not_granted).
sql_state_to_warning("S03", general_warning).
sql_state_to_warning("S04", general_warning).

:- pred sql_state_to_error(string::in, string::in, odbc.error::out) is semidet.

sql_state_to_error("07", "002", execution_error(incorrect_count_field)).
sql_state_to_error("07", "005", general_error).
sql_state_to_error("07", "006", execution_error(restricted_data_type_violation)).
sql_state_to_error("07", "009", general_error).
sql_state_to_error("07", "S01", internal_error).

sql_state_to_error("08", "001", connection_error(unable_to_establish)).
sql_state_to_error("08", "002", connection_error(connection_name_in_use)).
sql_state_to_error("08", "003", connection_error(nonexistent_connection)).
sql_state_to_error("08", "004", connection_error(connection_rejected_by_server)).
sql_state_to_error("08", "007", connection_error(connection_failure)).
sql_state_to_error("08", "S01", connection_error(connection_failure)).


sql_state_to_error("21", "S01", execution_error(invalid_insert_value_list)).
sql_state_to_error("21", "S02", execution_error(incorrect_derived_table_arity)).

sql_state_to_error("22", "001", execution_error(string_data_truncated)).
sql_state_to_error("22", "002", execution_error(general_error)).
sql_state_to_error("22", "003", execution_error(range_error)).
sql_state_to_error("22", "007", execution_error(invalid_date_time)).
sql_state_to_error("22", "008", execution_error(overflow)).
sql_state_to_error("22", "012", execution_error(division_by_zero)).
sql_state_to_error("22", "015", execution_error(overflow)).
sql_state_to_error("22", "018", execution_error(invalid_cast_specification)).
sql_state_to_error("22", "019", execution_error(invalid_escape)).
sql_state_to_error("22", "025", execution_error(invalid_escape)).
sql_state_to_error("22", "026", execution_error(string_data_length_mismatch)).

sql_state_to_error("23", "000", execution_error(integrity_constraint_violation)).

sql_state_to_error("24", "000", execution_error(general_error)).

sql_state_to_error("25", "S00", transaction_error(invalid_state)).
sql_state_to_error("25", "S01", transaction_error(invalid_state)).
sql_state_to_error("25", "S02", transaction_error(still_active)).
sql_state_to_error("25", "S03", transaction_error(rolled_back)).

sql_state_to_error("28", "000", connection_error(invalid_authorization)).

sql_state_to_error("37", "000", execution_error(syntax_error_or_access_violation)).

sql_state_to_error("3C", "000", execution_error(general_error)).

sql_state_to_error("3D", "000", execution_error(general_error)).

sql_state_to_error("3F", "000", execution_error(invalid_schema_name)).

sql_state_to_error("40", "001", transaction_error(serialization_failure)).
sql_state_to_error("40", "003", execution_error(general_error)).

sql_state_to_error("42", "000", execution_error(syntax_error_or_access_violation)).
sql_state_to_error("42", "S01", execution_error(table_or_view_already_exists)).
sql_state_to_error("42", "S02", execution_error(table_or_view_not_found)).
sql_state_to_error("42", "S11", execution_error(index_already_exists)).
sql_state_to_error("42", "S12", execution_error(index_not_found)).
sql_state_to_error("42", "S21", execution_error(column_already_exists)).
sql_state_to_error("42", "S22", execution_error(column_not_found)).

sql_state_to_error("44", "000", execution_error(general_error)).

sql_state_to_error("IM", Subcl, Err) :-
    ( if Subcl = "002" then Err = connection_error(unknown_data_source)
                       else Err = internal_error
    ).

sql_state_to_error("HY", SubClass, Error) :-
    ( SubClass = "000" ->
        Error = general_error
    ; SubClass = "109" ->
        Error = feature_not_implemented
    ; SubClass = "T00" ->
        Error = timeout_expired
    ; SubClass = "T01" ->
        Error = connection_error(timeout_expired)
    ;
        Error = internal_error
    ).

sql_state_to_error("S0", "001", execution_error(table_or_view_already_exists)).
sql_state_to_error("S0", "002", execution_error(table_or_view_not_found)).
sql_state_to_error("S0", "011", execution_error(index_already_exists)).
sql_state_to_error("S0", "012", execution_error(index_not_found)).
sql_state_to_error("S0", "021", execution_error(column_already_exists)).
sql_state_to_error("S0", "022", execution_error(column_not_found)).
sql_state_to_error("S0", "023", execution_error(no_default_for_column)).

sql_state_to_error("S1", SubClass, Error) :-
    ( SubClass = "000" ->
        Error = general_error
    ; SubClass = "C00" ->
        Error = feature_not_implemented
    ; SubClass = "T01" ->
        Error = connection_error(timeout_expired)
    ;
        Error = internal_error
    ).



%----------------------------------------------------------------------------------------------------
% Error Levels

% The "maximum" of two error levels - okay < warnings < errors

:- func max(error_level, error_level) = error_level.

max(okay,       okay)         = okay.
max(okay,       warnings)     = warnings.
max(okay,       errors)       = errors.
max(warnings,   okay)         = warnings.
max(warnings,   warnings)     = warnings.
max(warnings,   errors)       = errors.
max(errors,     okay)         = errors.
max(errors,     warnings)     = errors.
max(errors,     errors)       = errors.




% Filter a list of ODBC messages for the warnings, or for the errors, respectively.

warnings(L) =
    filter_map(
        func(odbc.warning(W) - Msgs) = W - Msgs is semidet,
        L).


errors(L) =
    filter_map(
        func(odbc.error(E) - Msgs) = E - Msgs is semidet,
        L).


is_error(error(_) - _).

is_warning(warning(_) - _).


%----------------------------------------------------------------------------------------------------

:- pred odbc.ok(int::in) is semidet.

:- pragma foreign_proc("C",
    odbc.ok(Status::in),
    [promise_pure, will_not_call_mercury, thread_safe],
"
    SUCCESS_INDICATOR =
        (Status == SQL_SUCCESS || Status == SQL_SUCCESS_WITH_INFO);
").



%----------------------------------------------------------------------------------------------------
% 9. Helpers for building SQL queries
%----------------------------------------------------------------------------------------------------


sqlquote0(Str) =
    string.join_list("", sqlquote_cl(string.to_char_list(Str))).


sqlquote_like0(Str) =
    string.join_list("", sqlquote_like_cl(string.to_char_list(Str))).


sqlquote(Str) =
    """" ++ sqlquote0(Str) ++ """".


% Character list version
:- func sqlquote_cl(list(char)) = list(string).

sqlquote_cl([]) = [].
sqlquote_cl([Ch|Chs]) =
    ( if
        ( Ch = '\'',     Q = "\\'"    %"
        ; Ch = '"',      Q = "\\\""   %"
        ; Ch = '\b',     Q = "\\b"
        ; Ch = '\n',     Q = "\\n"
        ; Ch = '\r',     Q = "\\r"
        ; Ch = '\t',     Q = "\\t"
        ; Ch = ('\\'),   Q = "\\\\"
        )
    then
        [ Q | sqlquote_cl(Chs) ]
    else
        [ char_to_string(Ch) | sqlquote_cl(Chs) ]
  ).



sqlquote_like(Str) =
    """" ++ sqlquote_like0(Str) ++ """".



% Character list version
:- func sqlquote_like_cl(list(char)) = list(string).

sqlquote_like_cl([]) = [].
sqlquote_like_cl([Ch|Chs]) =
    ( if
        ( Ch = '\'',     Q = "\\'"
        ; Ch = '"',      Q = "\\\""
        ; Ch = '\b',     Q = "\\b"
        ; Ch = '\n',     Q = "\\n"
        ; Ch = '\r',     Q = "\\r"
        ; Ch = '\t',     Q = "\\t"
        ; Ch = ('\\'),   Q = "\\\\"
        ; Ch = '%',      Q = "\\%"
        )
    then
        [ Q | sqlquote_like_cl(Chs) ]
    else
        [ char_to_string(Ch) | sqlquote_like_cl(Chs) ]
  ).



%----------------------------------------------------------------------------------------------------
% X. Control structures
%----------------------------------------------------------------------------------------------------

:- pred update_error_state(
    error_level::in, messages::in,
    odbc.state::di, odbc.state::uo
) is det.

update_error_state(ErrorLevel, Messages, !DB) :-
    !.DB = odbc.state(Env, Conn, ErrorLevel1, Messages1),
    unsafe_promise_unique(odbc.state(Env, Conn, ErrorLevel2, Messages2), !:DB),
    ErrorLevel2 = max(ErrorLevel1, ErrorLevel),
    Messages2 = Messages ++ Messages1.


:- pred chain(
    pred(T, odbc.statement, odbc.statement, odbc.state, odbc.state) :: pred(out, di, uo, di, uo) is det,
    T::out,
    T::in,
    odbc.statement::di, odbc.statement::uo,
    odbc.state::di, odbc.state::uo
) is det.

chain(Pred, Out, FallBack, !Statement, !DB) :-
    !.DB = odbc.state(_, _, ErrorLevel, _),
    unsafe_promise_unique(!.DB, !:DB),
    (
        if
            ErrorLevel = errors
        then
            Out = FallBack
        else
            Pred(Out, !Statement, !DB)
   ).


:- pred chain(
    pred(odbc.statement, odbc.statement, odbc.state, odbc.state) :: pred(di, uo, di, uo) is det,
    odbc.statement::di, odbc.statement::uo,
    odbc.state::di, odbc.state::uo
) is det.

chain(Pred, !Statement, !DB) :-
    !.DB = odbc.state(_, _, ErrorLevel, _),
    unsafe_promise_unique(!.DB, !:DB),
    (
        if
            ErrorLevel = errors
        then
            true
        else
            Pred(!Statement, !DB)
   ).



% Clear the error state inside the database state. This is needed when a new command is executed, so it won't
% collect all the warnings and errors from the previous command, which have already returned inside an
% odbc.result/0 or odbc.result/1. clear/4 also returns the environment and connection handles, which are often
% needed as well.

:- pred clear(
    odbc.state::di, odbc.state::uo,
    odbc.env::uo,
    odbc.conn::uo
) is det.

clear(!DB, Env, Conn) :-
    !.DB = odbc.state(Env, Conn, _, _),
    !:DB = odbc.state(Env, Conn, okay, []),
    unsafe_promise_unique(!.DB, !:DB).



%----------------------------------------------------------------------------------------------------
% Y. Debugging
%----------------------------------------------------------------------------------------------------

:- pred debug(odbc.state::di, odbc.state::uo, string::in) is det.

debug(!DB, Txt) :-
    !.DB = state(_,_,ErrorLevel, Messages),
    trace [io(!IO)] (
        io.format("%s:\n    ErrorLevel = %s\n    Messages = [ ", [s(Txt), s(string(ErrorLevel))], !IO),
        io.write_list(Messages, ",\n             ", io.print, !IO),
        io.write_string(" ]\n", !IO)
    ),
    unsafe_promise_unique(!.DB, !:DB).
