# Parallel Performance under MySQL/MariaDB
{:.no_toc}

* I had a much higher speed when I switched from many small transactions to few large ones. Before, the CPU load
  was ridiculously low. Afters, it reached 50% to 100% on all hardware threads.

* There wasn't much of a speed increase, when I made every thread have its own database connection, as opposed to
  sharing a connection between the threads.
