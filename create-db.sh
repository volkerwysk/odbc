#! /bin/bash

# This creates the ODBC test database and a user.
# Database name = odbctest
# User name = odbctest
# Password = odbctest

echo "DROP DATABASE odbctest;" | sudo mysql

echo "CREATE DATABASE odbctest CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;" | sudo mysql
echo "CREATE USER 'odbctest'@'localhost' IDENTIFIED BY 'odbctest';" | sudo mysql
echo "GRANT ALL ON odbctest.* TO 'odbctest'@'localhost';" | sudo mysql
echo "FLUSH PRIVILEGES;" | sudo mysql
