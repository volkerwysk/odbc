#! /bin/bash

PROG="$1"

if [ "$1" == "" ] ; then
    PROG=test
fi

# This doesn't help:
# export LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu

# I don't know why I have to specify this all of a sudden (2024-01-20):
# -L /usr/lib/x86_64-linux-gnu/

mmc --make \
    -L /usr/lib/x86_64-linux-gnu/ \
    --grade asm_fast.par.gc.stseg \
    -j16 -E --output-compile-error-lines=200 \
    -lpthread \
    --cflag "-DMODBC_UNIX" --cflag "-DMODBC_MYSQL" \
    --cflags "$(pkg-config --cflags odbc)" \
    `pkg-config --libs odbc --static` \
    --mercury-linkage shared \
    --error-files-in-subdir \
    --max-error-line-width=115 \
    --no-color-diagnostics \
    --output-compile-error-lines=200 \
    "$PROG"

# For Emacs. Can be removed.
etags *.m

exit 0
    --mercury-linkage static \
#    -L /usr/lib/x86_64-linux-gnu/ \
