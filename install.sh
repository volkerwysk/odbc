#! /bin/bash

# This doesn't work. When this has been done, linking failed, both static and shared. I had to 
# remove the odbc library from the mercury main directroy (and reinstalled shared) for it to work again:

# mmc --make -j16 \
#     -lodbc -lpthread \
#     --cflag "-DMODBC_UNIX" --cflag "-DMODBC_MYSQL" \
#     --mercury-linkage static \
#     libodbc.install

# This works, shared linking only:

mmc --make -j16 \
    -L /usr/lib/x86_64-linux-gnu/ \
    -lodbc -lpthread \
    --cflag "-DMODBC_UNIX" --cflag "-DMODBC_MYSQL" \
    libodbc.install


#    --mercury-linkage shared \
#    --cflags "$(pkg-config --cflags odbc)" \
