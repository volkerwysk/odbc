#! /bin/bash

for MD in *.md ; do
    BASIS=`basename "$MD" .md`
    echo "$MD"
    kramdown --template=document-with-css.html "$MD" > "$BASIS.html"
done
